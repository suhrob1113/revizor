// Dart imports:
import 'dart:convert';
import 'dart:io';

// Package imports:
import 'package:get_it/get_it.dart';
import 'package:pausable_timer/pausable_timer.dart';

// Project imports:
import 'package:revizor/api/models/appraisal/appraisal.dart';
import 'package:revizor/api/services/appraisal/custom_appraisal_api.dart';
import 'package:revizor/redux/appraisal/actions.dart';
import 'package:revizor/redux/appraisal/store.dart';
import 'package:revizor/ui/pop_ups/didnt_liked_today_pop_up.dart';
import 'package:revizor/ui/pop_ups/thank_you_pop_up.dart';
import 'package:revizor/utils/connectivity_helper.dart';
import 'package:revizor/utils/shared_preference_helper.dart';
import 'root_service.dart';

class AppraisalService {
  static void init() {
    final getIt = GetIt.instance;

    getIt.registerSingleton<AppraisalService>(AppraisalService());
    getIt<AppraisalService>().create();
  }

  PausableTimer? pausableTimer;
  bool checkingUnsuccessfulAppraisals = false;
  // Timer? _timer;

  void create() {
    pausableTimer = PausableTimer(Duration(seconds: 10), () {});
    checkUnsuccessfulAppraisals();
  }

  AppraisalStore get _store => RootService.stores.appraisal;
  // BehaviorSubject<AppraisalState> _behaviorSubject;

  void dispose() {
    //   _behaviorSubject.close();
  }

  void updateRating(int rating, int id, [bool nps = false]) async {
    if (rating == 5 && !nps) {
      _store.dispatch(UpdateRatingAction(rating, id));
      _store.dispatch(SendRatingAction());
      RootService.navigator.myDialog(ThankYouPopUp(), hasTitleAndBack: false);
      prolongAndClose();
    } else {
      _store.dispatch(UpdateRatingAction(rating, id));
      RootService.navigator.myDialog(DidntLikedTodayPopUp(),
          title: RootService.systext.getValue(nps
              ? 'didnt-like-popup.why-did-you-vote-that-way'
              : 'didnt-like-popup.is-there-anything-you-didnt-like-today'));
      prolongAndSend();
    }
  }

  void updatePhoneNumber(String phoneNumber) {
    _store.dispatch(UpdatePhoneNumberAction(phoneNumber));
    prolongAndSend();
  }

  void updateComment(String comment) {
    _store.dispatch(UpdateCommentAction(comment));
    prolongAndSend();
  }

  void updateAudioPath(String audioPath) {
    _store.dispatch(UpdateAudioPathAction(audioPath));
    prolongAndSend();
  }

  void updateVideoPath(String videoPath) {
    _store.dispatch(UpdateVideoPathAction(videoPath));
    prolongAndSend();
  }

  void stopTimer() {
    pausableTimer!.cancel();
  }

  void sendRating() async {
    stopTimer();
    _store.dispatch(SendRatingAction());
  }

  void updateAnswer(int answer) {
    _store.dispatch(UpdateAnswerAction(answer));
  }

  void prolongAndSend() {
    if (pausableTimer != null) pausableTimer!.cancel();
    pausableTimer = PausableTimer(const Duration(seconds: 10), () async {
      sendRating();
      RootService.navigator.popUNtilFirst();
    });
    pausableTimer!.start();
  }

  void prolongAndClose() {
    if (pausableTimer != null) pausableTimer!.cancel();
    pausableTimer = PausableTimer(const Duration(seconds: 10), () async {
      RootService.navigator.popUNtilFirst();
    });
    pausableTimer!.start();
  }

  void prolongAndSendAnswer() {
    if (pausableTimer != null) pausableTimer!.cancel();
    pausableTimer = PausableTimer(const Duration(seconds: 10), () async {
      sendRating();
    });
    pausableTimer!.start();
  }

  void prolongAndResetAnswers() {
    if (pausableTimer != null) pausableTimer!.cancel();
    pausableTimer = PausableTimer(const Duration(seconds: 10), () async {
      RootService.tablet.selectAnswer(null);
    });
    pausableTimer!.start();
  }

  void checkUnsuccessfulAppraisals() async {
    if (!checkingUnsuccessfulAppraisals) {
      checkingUnsuccessfulAppraisals = true;
      if (await ConnectivityHelper.isConnected()) {
        List<String> unsuccessfulAppraisalList = SPHelper.unsuccessfulAppraisalList;
        print('unsuccessfulAppraisalList: ${unsuccessfulAppraisalList.length}');
        if (unsuccessfulAppraisalList.isNotEmpty) {
          unsuccessfulAppraisalList.forEach(
            (appraisalStirng) async {
              final map = json.decode(appraisalStirng);
              final appraisal = Appraisal.fromJson(map);
              if (await CustomAppraisalApi.customAppraisalRequest(appraisal)) {
                if (appraisal.audioPath != null) {
                  final file = File(appraisal.audioPath!);
                  await file.delete();
                }
                if (appraisal.videoPath != null) {
                  final file = File(appraisal.videoPath!);
                  await file.delete();

                  unsuccessfulAppraisalList.removeWhere((element) => element == appraisalStirng);
                  SPHelper.unsuccessfulAppraisalList = unsuccessfulAppraisalList;
                }
              } else {
                checkingUnsuccessfulAppraisals = false;
                return;
              }
            },
          );
        }
      }
      checkingUnsuccessfulAppraisals = false;
    }
  }
}
