// Package imports:
import 'package:connectivity/connectivity.dart';
import 'package:get_it/get_it.dart';

// Project imports:
import 'package:revizor/services/root_service.dart';

// Project imports:

class ConnectivityService {
  static void init() {
    final getIt = GetIt.instance;

    getIt.registerSingleton<ConnectivityService>(ConnectivityService());
    getIt<ConnectivityService>().create();
  }

  ConnectivityResult? _last;

  void create() {
    Connectivity().onConnectivityChanged.listen(onChange);
  }

  void onChange(ConnectivityResult event) async {
    if ((_last == null || _last == ConnectivityResult.none) && event != ConnectivityResult.none) {
      if (!firstLaunch) {
        RootService.tablet.fetch();
        RootService.appraisal.checkUnsuccessfulAppraisals();
      }
      firstLaunch = false;
    }

    _last = event;
  }

  bool firstLaunch = true;
}
