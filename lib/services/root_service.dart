// Package imports:
import 'package:get_it/get_it.dart';

// Project imports:
import 'package:revizor/api/api_provider.dart';
import 'package:revizor/redux/stores.dart';
import 'package:revizor/services/appraisal_service.dart';
import 'package:revizor/services/authentication_service.dart';
import 'package:revizor/services/error_service.dart';
import 'package:revizor/services/systext_service.dart';
import 'package:revizor/services/tablet_service.dart';
import 'connectivity_service.dart';
import 'navigator_service.dart';

import 'package:revizor/services/socket_service.dart'
    if (dart.library.js) 'package:revizor/services/socket_service_for_web.dart';

// Project imports:

class RootService {
  //
  static final _getIt = GetIt.instance;

  static void init() {
    if (!_getIt.isRegistered<RootService>()) {
      _getIt.registerSingleton<RootService>(RootService());
      _getIt<RootService>().initServices();
    }
  }

  void initServices() {
    Stores.init();

    SocketService.init();
    ApiProvider.init();

    SystextService.init();
    AuthenticationService.init();
    TabletService.init();
    AppraisalService.init();

    NavigatorService.init();
    ConnectivityService.init();
    ErrorService.init();
  }

  static ApiProvider get api => _getIt<ApiProvider>();

  static Stores get stores => _getIt<Stores>();

  static NavigatorService get navigator => _getIt<NavigatorService>();

  static AuthenticationService get authentication => _getIt<AuthenticationService>();

  static TabletService get tablet => _getIt<TabletService>();

  static AppraisalService get appraisal => _getIt<AppraisalService>();

  static SystextService get systext => _getIt<SystextService>();

  static SocketService get socket => _getIt<SocketService>();

  static void start() {
    tablet.fetch();
    systext.fetch();
  }

  static void beginSocketService(int serviceId) {
    socket.beginListenning(serviceId);
  }
}
