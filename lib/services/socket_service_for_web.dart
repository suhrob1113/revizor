// Dart imports:
import 'dart:convert';
import 'dart:html';

// Package imports:
import 'package:get_it/get_it.dart';
import 'package:revizor/conf/routes/main_routes_constants.dart';
import 'package:revizor/conf/sp_keys/shared_preference_constants.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

// Project imports:
import 'package:revizor/services/root_service.dart';

// Project imports:

class SocketService {
  WebSocket? webSocket;
  // HtmlWebSocketChannel channel = HtmlWebSocketChannel.connect(Uri.parse('wss://lagoms.uz:8001/ws/service/1/'));
  static void init() {
    final getIt = GetIt.instance;

    getIt.registerSingleton<SocketService>(SocketService());
  }

  int currentServiceId = 0;

  void beginListenning(int serviceId) async {
    if (webSocket != null) webSocket!.close();

    webSocket = WebSocket('ws://lagoms.uz:8001/ws/service/$serviceId/');
    print('listening to service: $serviceId');

    webSocket!.onMessage.listen((message) {
      final map = json.decode(message.data);
      print(message);
      switch (map['message_type']) {
        case 'data':
          print(map);
          print('data');
          RootService.tablet.fetch();

          break;
        case 'system_texts':
          RootService.systext.fetch();
          break;
        case 'logout':
          SPHelper.remove(SPKeys.accessType);
          RootService.navigator.popUNtilFirst();
          RootService.navigator.pushReplacementNamed(MyRoutes.authenticationPage);
          break;
        default:
      }
    });

    webSocket!.onError.listen((event) {
      print(event);
    });

    // currentServiceId = serviceId;
    // channel!.stream.listen((message) {
    //   final map = json.decode(message);
    //   print(message);
    //   switch (map['message_type']) {
    //     case 'data':
    //       if (!updating) {
    //         print(map);
    //         updating = true;
    //         print('data');
    //         RootService.tablet.fetch();
    //         Future.delayed(Duration(seconds: 15), () => updating = false);
    //       }
    //       break;
    //     case 'system_texts':
    //       RootService.systext.fetch();
    //       break;
    //     case 'logout':
    //       SPHelper.remove(SPKeys.accessType);
    //       RootService.navigator.popUNtilFirst();
    //       RootService.navigator.pushReplacementNamed(MyRoutes.authenticationPage);
    //       break;
    //     default:
    //   }
    // }, onError: (error) {
    //   print(error);
    // });
  }
}
