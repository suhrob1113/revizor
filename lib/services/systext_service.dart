// Package imports:
import 'package:get_it/get_it.dart';
import 'package:rxdart/rxdart.dart';

// Project imports:
import 'package:revizor/api/models/systext/systext.dart';
import 'package:revizor/redux/systext/actions.dart';
import 'package:revizor/redux/systext/store.dart';
import 'root_service.dart';

class SystextService {
  static void init() {
    final getIt = GetIt.instance;

    getIt.registerSingleton<SystextService>(SystextService());
    getIt<SystextService>().create();
  }

  void create() {
    _store.state$.map((state) => state.systexts).pipe(_systexts);
    fetch();
  }

  void fetch() {
    _store.dispatch(FetchSystextsAction());
  }

  final _systexts = BehaviorSubject<List<Systext>?>();
  BehaviorSubject<List<Systext>?> get systexts$ => _systexts;
  SystextStore get _store => RootService.stores.systext;

  List<Systext> get systexts => systexts$.value ?? [];

  String getValue(String key) {
    return systexts.firstWhere((element) => element.key == key, orElse: () => Systext(value: '')).value ?? '';
  }

  void dispose() {
    _systexts.close();
  }
}
