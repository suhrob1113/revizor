// Dart imports:
import 'dart:convert';

// Package imports:
import 'package:get_it/get_it.dart';
import 'package:revizor/conf/routes/main_routes_constants.dart';
import 'package:revizor/conf/sp_keys/shared_preference_constants.dart';
import 'package:revizor/utils/shared_preference_helper.dart';
import 'package:web_socket_channel/io.dart';

// Project imports:
import 'package:revizor/services/root_service.dart';

// Project imports:

class SocketService {
  IOWebSocketChannel channel = IOWebSocketChannel.connect(Uri.parse('ws://lagoms.uz:8001/ws/service/1/'));
  static void init() {
    final getIt = GetIt.instance;

    getIt.registerSingleton<SocketService>(SocketService());
  }

  int currentServiceId = 0;

  void beginListenning(int serviceId) async {
    if (currentServiceId != serviceId && serviceId != 1) {
      await channel.sink.close();
      try {
        channel = IOWebSocketChannel.connect(Uri.parse('ws://lagoms.uz:8001/ws/service/$serviceId/'));
        print('listening to service: $serviceId');
        currentServiceId = serviceId;
        channel.stream.listen((message) {
          final map = json.decode(message);
          print(message);
          switch (map['message_type']) {
            case 'data':
              RootService.tablet.fetch();
              break;
            case 'system_texts':
              RootService.systext.fetch();
              break;
            case 'logout':
              SPHelper.remove(SPKeys.accessType);
              RootService.navigator.popUNtilFirst();
              RootService.navigator.pushReplacementNamed(MyRoutes.authenticationPage);
              break;
            default:
          }
        }, onError: (error) {
          print(error);
        });
      } catch (e) {
        print(e);
      }
    }
  }
}
