// Package imports:
import 'package:get_it/get_it.dart';

// Project imports:
import 'package:revizor/api/models/answer/answer.dart';
import 'package:revizor/api/models/department/department.dart';
import 'package:revizor/api/models/employee/employee.dart';
import 'package:revizor/api/models/questionnaire/questionnaire.dart';
import 'package:revizor/api/models/service_info/service_info.dart';
import 'package:revizor/redux/tablet/actions.dart';
import 'package:revizor/redux/tablet/store.dart';
import 'package:revizor/utils/shared_preference_helper.dart';
import 'root_service.dart';

class TabletService {
  static void init() {
    final getIt = GetIt.instance;

    getIt.registerSingleton<TabletService>(TabletService());
    getIt<TabletService>().create();
  }

  void create() {
    if (SPHelper.accessToken != null) fetch();
  }

  void fetch() {
    _store.dispatch(FetchTabletAction());
  }

  void selectEmployee(Employee employee) {
    _store.dispatch(SelectEmployeeAction(employee));
  }

  void selectDepartment(Department department) {
    _store.dispatch(SelectDepartmentAction(department));
  }

  void selectServiceInfo(ServiceInfo serviceInfo) {
    _store.dispatch(SelectServiceInfoAction(serviceInfo));
  }

  void selectAnswer(Questionnaire? questionnaire) {
    _store.dispatch(SelectQuestionnaireAction(questionnaire));
  }

  TabletStore get _store => RootService.stores.tablet;
}
