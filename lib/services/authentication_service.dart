// Package imports:
import 'package:get_it/get_it.dart';

// Project imports:
import 'package:revizor/redux/authentication/actions.dart';
import 'package:revizor/redux/authentication/store.dart';
import 'root_service.dart';

class AuthenticationService {
  static void init() {
    final getIt = GetIt.instance;

    getIt.registerSingleton<AuthenticationService>(AuthenticationService());
    getIt<AuthenticationService>().create();
  }

  void create() {}

  void authenticate() {
    _store.dispatch(AuthenticateAction());
  }

  void updateUsername(String username) {
    _store.dispatch(UpdateUsernameAction(username));
  }

  void updatePassword(String password) {
    _store.dispatch(UpdatePasswordAction(password));
  }

  AuthenticationStore get _store => RootService.stores.authentication;
}
