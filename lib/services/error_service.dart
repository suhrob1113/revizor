import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:revizor/redux/appraisal/actions.dart';
import 'package:revizor/redux/appraisal/store.dart';
import 'package:revizor/redux/authentication/actions.dart';
import 'package:revizor/redux/authentication/store.dart';
import 'package:revizor/redux/systext/actions.dart';
import 'package:revizor/redux/systext/store.dart';
import 'package:revizor/redux/tablet/actions.dart';
import 'package:revizor/redux/tablet/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:rxdart/rxdart.dart';

class ErrorService {
  //
  static void init() {
    final getIt = GetIt.instance;

    getIt.registerSingleton<ErrorService>(ErrorService());
    getIt<ErrorService>().create();
  }

  late BehaviorSubject<AuthenticationState> _authenticationStream;
  late BehaviorSubject<TabletState> _tabletStream;
  late BehaviorSubject<AppraisalState> _appraisalStream;
  late BehaviorSubject<SystextState> _systextStream;

  bool showingErrorDialog = false;

  void create() {
    _authenticationStream = RootService.stores.authentication.state$;
    _tabletStream = RootService.stores.tablet.state$;
    _appraisalStream = RootService.stores.appraisal.state$;
    _systextStream = RootService.stores.systext.state$;

    _authenticationStream.listen(
      (state) async {
        if (state.action is AuthenticationErrorAction) {
          _showErrorDialog(state.errorMessage);
        }
      },
    );
    _systextStream.listen(
      (state) async {
        if (state.action is SystextErrorAction) {
          _showErrorDialog(state.errorMessage);
        }
      },
    );
    _appraisalStream.listen(
      (state) async {
        if (state.action is AppraisalErrorAction) {
          _showErrorDialog(state.errorMessage);
        }
      },
    );
    _tabletStream.listen(
      (state) async {
        if (state.action is TabletErrorAction) {
          _showErrorDialog(state.errorMessage);
        }
      },
    );
  }

  void _showErrorDialog(String? message) async {
    if (!showingErrorDialog) {
      showingErrorDialog = true;
      await RootService.navigator
          .customDialog(
            AlertDialog(
              content: Text(
                message ?? 'Something went wrong',
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      RootService.navigator.pop();
                    },
                    child: Icon(Icons.close))
              ],
            ),
          )
          .then((value) => showingErrorDialog = false);
    }
  }

  void dispose() async {
    await _authenticationStream.drain();
    await _tabletStream.drain();
    await _appraisalStream.drain();
    await _systextStream.drain();
    _authenticationStream.close();
    _tabletStream.close();
    _appraisalStream.close();
    _systextStream.drain();
  }
}
