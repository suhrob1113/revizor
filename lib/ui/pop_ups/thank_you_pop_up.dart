// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/conf/assets/image_constants.dart';
import 'package:revizor/conf/values/strings_constants.dart';
import 'package:revizor/redux/systext/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/utils/media_helper.dart';

class ThankYouPopUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaHelper.height - 250,
      width: MediaHelper.width,
      child: StreamBuilder<SystextState>(
          stream: RootService.stores.systext.state$,
          builder: (context, snapshot) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MyText(
                      RootService.systext.getValue('thnak-you-popup.thank-you'),
                      fontSize: 48,
                      height: 62 / 48,
                      fontWeight: FontWeight.w700,
                    ),
                  ],
                ),
                Spacer(
                  flex: 2,
                ),
                MyText(
                  RootService.systext.getValue('thnak-you-popup.only-one-out-of-4'),
                  fontSize: 24,
                  height: 32 / 24,
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                Expanded(
                  flex: 20,
                  child: Image.asset(
                    MyImages.salute,
                    fit: BoxFit.cover,
                  ),
                ),
                Spacer(),
                MyText(
                  MyStrings.ok,
                  onTap: onOk,
                  fontSize: 28,
                  height: 36 / 28,
                  fontWeight: FontWeight.w700,
                )
              ],
            );
          }),
    );
  }

  void onOk() {
    RootService.navigator.popUNtilFirst();
    RootService.appraisal.stopTimer();
  }
}
