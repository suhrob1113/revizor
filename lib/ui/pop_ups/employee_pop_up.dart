// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/redux/tablet/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/states/loading_state.dart';
import 'package:revizor/ui/employee/employee_item.dart';
import 'package:revizor/ui/smileys/smileys.dart';
import 'package:revizor/utils/media_helper.dart';

class EmployeePopUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<TabletState>(
      stream: RootService.stores.tablet.state$,
      builder: (context, snapshot) {
        final state = snapshot.data;
        final employee = state?.selectedEmployee;
        if (state == null || employee == null) return MyLoadingState();

        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            EmployeeItem(
              name: employee.name!,
              position: employee.position!,
              stars: employee.rating!.toInt(),
              image: employee.photo!,
              isBig: MediaHelper.height >= 600 ? true : false,
            ),
            Smileys(
              id: employee.id!,
            ),
          ],
        );
      },
    );
  }
}
