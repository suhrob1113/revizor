import 'package:flutter/material.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_image.dart';
import 'package:revizor/utils/media_helper.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class ImageFullWidthPopUp extends StatelessWidget {
  final String imageUrl;

  const ImageFullWidthPopUp({Key? key, required this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyContainer(
        width: MediaHelper.width,
        height: MediaHelper.height - 180,
        padding: const EdgeInsets.all(10),
        color: SPHelper.themeId == 1
            ? MyColors.sailor
            : SPHelper.themeId == 2
                ? MyColors.white
                : MyColors.accent,
        child: MyImage(
          imageUrl,
          width: MediaHelper.width,
          fit: BoxFit.cover,
        ));
  }
}
