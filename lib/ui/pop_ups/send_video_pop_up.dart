// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';

// Project imports:
import 'package:revizor/conf/assets/icon_constants.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/redux/systext/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/box_decorations.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_icon_asset.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/theme/default/default_text_field.dart';
import 'package:revizor/ui/pop_ups/thank_you_pop_up.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class SendVideoPopUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<SystextState>(
        stream: RootService.stores.systext.state$,
        builder: (context, snapshot) {
          return Flexible(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                // shrinkWrap: true,
                children: [
                  MyContainer(
                    borderRadius: MyBorderRadius.allRounded150,
                    width: 140,
                    hasShadow: SPHelper.themeId == 2 ? true : false,
                    color: SPHelper.themeId == 1 ? MyColors.sailorLight : MyColors.white,
                    padding: const EdgeInsets.all(37),
                    child: MyIconAsset(
                      MyIcons.takeVideo,
                      size: 66,
                    ),
                  ),
                  SizedBox(height: 24),
                  SizedBox(
                    width: 500,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        MyText(
                          RootService.systext.getValue('send-video-popup.leave-your-phone-number'),
                          fontSize: 20,
                        ),
                        SizedBox(height: 8),
                        MyContainer(
                          child: MyTextField(
                            onChanged: RootService.appraisal.updatePhoneNumber,
                            prefixText: '+998 ',
                            maxLines: 1,
                            keyboardType: TextInputType.phone,
                            inputFormatters: [MaskedInputFormatter('00 000 00 00')],
                          ),
                          color: SPHelper.themeId == 3 ? MyColors.accentDark : null,
                          borderRadius: MyBorderRadius.allRounded15,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 32),
                  MyText(
                    RootService.systext.getValue('send-video-popup.send'),
                    onTap: () => onSend(context),
                    fontSize: 26,
                    fontWeight: FontWeight.w700,
                  )
                ],
              ),
            ),
          );
        });
  }

  void onSend(BuildContext context) async {
    RootService.appraisal.sendRating();
    RootService.navigator.popUNtilFirst();
    RootService.navigator.myDialog(ThankYouPopUp(), hasTitleAndBack: false);
    RootService.appraisal.prolongAndClose();
  }
}
