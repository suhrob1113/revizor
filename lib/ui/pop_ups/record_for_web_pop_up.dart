// Dart imports:
import 'dart:async';
// Flutter imports:
import 'package:flutter/material.dart';
import 'package:microphone/microphone.dart';
//import 'package:flutter_sound/flutter_sound.dart';

// Package imports:
// Project imports:
import 'package:revizor/conf/assets/image_constants.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/custom/ripples_animation.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/ui/pop_ups/send_record_pop_up.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class RecordForWebPopUp extends StatefulWidget {
  @override
  _RecordForWebPopUpState createState() => _RecordForWebPopUpState();
}

class _RecordForWebPopUpState extends State<RecordForWebPopUp> with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  // FlutterSoundRecorder _myRecorder = FlutterSoundRecorder();

  bool flag = true;
  Stream<int>? timerStream;
  StreamSubscription<int>? timerSubscription;
  String minutesStr = '00';
  String secondsStr = '00';
  String audioPath = '';
  final microphoneRecorder = MicrophoneRecorder();
  late Timer timer;

  bool _microphoneIsInited = false;

  StreamController<int>? streamController;
  Stream<int> stopWatchStream() {
    Timer? timer;
    Duration timerInterval = Duration(seconds: 1);
    int counter = 0;

    void stopTimer() {
      if (timer != null) {
        timer!.cancel();
        timer = null;
        counter = 0;
        streamController!.close();
      }
    }

    void tick(_) {
      counter++;
      streamController!.add(counter);
      if (!flag) {
        stopTimer();
      }
    }

    void startTimer() {
      timer = Timer.periodic(timerInterval, tick);
    }

    streamController = StreamController<int>(
      onListen: startTimer,
      onCancel: stopTimer,
      onResume: startTimer,
      onPause: stopTimer,
    );

    return streamController!.stream;
  }

  void beginTimer() {
    timerStream = stopWatchStream();
    timerSubscription = timerStream!.listen(
      (int newTick) {
        setState(
          () {
            minutesStr = ((newTick / 60) % 60).floor().toString().padLeft(2, '0');
            secondsStr = (newTick % 60).floor().toString().padLeft(2, '0');
          },
        );
      },
    );
  }

  Future<void> stopRecording() async {
    try {
      if (!microphoneRecorder.value.stopped) await microphoneRecorder.stop();
    } catch (e) {}
  }

  @override
  void initState() {
    microphoneRecorder.init().then((value) {
      microphoneRecorder.start();
      _controller = AnimationController(vsync: this, duration: Duration(milliseconds: 2000))..repeat();

      beginTimer();
      timer = Timer(Duration(seconds: 120), () async {
        await stopRecording();
        RootService.appraisal.updateAudioPath(microphoneRecorder.value.recording?.url ?? '');
        RootService.appraisal.sendRating();
        RootService.navigator.popUNtilFirst();
      });

      _microphoneIsInited = true;
    });

    super.initState();
  }

  @override
  void deactivate() async {
    timer.cancel();
    await stopRecording();

    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    timerSubscription?.cancel();
    streamController?.close();
    microphoneRecorder.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _microphoneIsInited
        ? Column(
            children: [
              Stack(
                alignment: Alignment.center,
                children: [
                  AnimatedBuilder(
                    animation: _controller,
                    builder: (context, child) {
                      return SizedBox(
                        height: 200,
                        width: 200,
                        child: CustomPaint(
                          painter: RipplesAnimation(
                            _controller.value,
                            count: 5,
                          ),
                        ),
                      );
                    },
                  ),
                  GestureDetector(
                    onTap: () => onPause(context),
                    child: Image.asset(
                      SPHelper.themeId == 3 ? MyImages.pauseWhite : MyImages.pause,
                      width: 110,
                      height: 110,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 12),
              MyText(
                "$minutesStr:$secondsStr",
                fontSize: 36,
                color: SPHelper.themeId == 1
                    ? MyColors.grey30
                    : SPHelper.themeId == 2
                        ? MyColors.primary35
                        : MyColors.white40,
              )
            ],
          )
        : Center(
            child: CircularProgressIndicator(),
          );
  }

  void onPause(BuildContext context) async {
    timer.cancel();
    await stopRecording();
    if (RootService.navigator.canPop()) await RootService.navigator.pop();

    RootService.appraisal.updateAudioPath(microphoneRecorder.value.recording?.url ?? '');
    RootService.navigator.myDialog(SendRecordPopUp(),
        title: RootService.systext.getValue('didnt-like-popup.is-there-anything-you-didnt-like-today') +
            '\n' +
            RootService.systext.getValue('didnt-like-popup.leave-an-audio-recording'));
  }
}
