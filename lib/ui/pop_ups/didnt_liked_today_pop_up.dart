// Flutter imports:
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';

// Package imports:
import 'package:permission_handler/permission_handler.dart';

// Project imports:
import 'package:revizor/conf/assets/icon_constants.dart';
import 'package:revizor/conf/routes/main_routes_constants.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/redux/systext/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/box_decorations.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_icon_asset.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/ui/pop_ups/record_for_web_pop_up.dart';
import 'package:revizor/ui/pop_ups/record_pop_up.dart';
import 'package:revizor/ui/pop_ups/send_comment_pop_up.dart';
import 'package:revizor/ui/pop_ups/thank_you_pop_up.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class DidntLikedTodayPopUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MyContainer(
      padding: const EdgeInsets.all(62),
      color: SPHelper.themeId == 1
          ? MyColors.sailor
          : SPHelper.themeId == 2
              ? MyColors.white
              : MyColors.accent,
      child: StreamBuilder<SystextState>(
        stream: RootService.stores.systext.state$,
        builder: (context, snapshot) {
          return Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Spacer(
                flex: 3,
              ),
              Column(
                children: [
                  MyContainer(
                    onTap: () => checkMicrophonePermission(context),
                    borderRadius: MyBorderRadius.allRounded150,
                    height: 140,
                    width: 140,
                    hasShadow: SPHelper.themeId == 2 ? true : false,
                    color: SPHelper.themeId == 1
                        ? MyColors.sailorLight
                        : SPHelper.themeId == 2
                            ? MyColors.white
                            : MyColors.white,
                    child: Center(
                      child: MyIconAsset(
                        MyIcons.record,
                        size: 66,
                      ),
                    ),
                  ),
                  SizedBox(height: 24),
                  SizedBox(
                    width: 160,
                    child: MyText(
                      RootService.systext.getValue('didnt-like-popup.i-want-to-say'),
                      fontSize: 22,
                      fontWeight: FontWeight.w700,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              Spacer(
                flex: 2,
              ),
              if (true) ...[
                Column(
                  children: [
                    MyContainer(
                      onTap: () => checkCameraPermission(context),
                      borderRadius: MyBorderRadius.allRounded150,
                      height: 140,
                      width: 140,
                      hasShadow: SPHelper.themeId == 2 ? true : false,
                      color: SPHelper.themeId == 1
                          ? MyColors.sailorLight
                          : SPHelper.themeId == 2
                              ? MyColors.white
                              : MyColors.white,
                      child: Center(
                        child: MyIconAsset(
                          MyIcons.takeVideo,
                          size: 66,
                        ),
                      ),
                    ),
                    SizedBox(height: 24),
                    SizedBox(
                      width: 160,
                      child: MyText(
                        RootService.systext.getValue('didnt-like-popup.i-want-to-record-a-video'),
                        fontSize: 22,
                        fontWeight: FontWeight.w700,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
                Spacer(
                  flex: 2,
                ),
              ],
              Column(
                children: [
                  MyContainer(
                    onTap: () => onWrite(context),
                    borderRadius: MyBorderRadius.allRounded150,
                    height: 140,
                    width: 140,
                    hasShadow: SPHelper.themeId == 2 ? true : false,
                    color: SPHelper.themeId == 1
                        ? MyColors.sailorLight
                        : SPHelper.themeId == 2
                            ? MyColors.white
                            : MyColors.white,
                    child: Center(
                      child: MyIconAsset(
                        MyIcons.write,
                        size: 66,
                      ),
                    ),
                  ),
                  SizedBox(height: 24),
                  SizedBox(
                    width: 160,
                    child: MyText(
                      RootService.systext.getValue('didnt-like-popup.i-want-to-write'),
                      fontSize: 22,
                      fontWeight: FontWeight.w700,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              Spacer(
                flex: 2,
              ),
              Column(
                children: [
                  MyContainer(
                    onTap: onDenied,
                    borderRadius: MyBorderRadius.allRounded150,
                    height: 140,
                    width: 140,
                    hasShadow: SPHelper.themeId == 2 ? true : false,
                    color: SPHelper.themeId == 1
                        ? MyColors.sailorLight
                        : SPHelper.themeId == 2
                            ? MyColors.white
                            : MyColors.white,
                    child: Center(
                      child: MyIconAsset(
                        MyIcons.denied,
                        size: 66,
                      ),
                    ),
                  ),
                  SizedBox(height: 24),
                  SizedBox(
                    width: 160,
                    child: MyText(
                      RootService.systext.getValue('didnt-like-popup.no-comments'),
                      fontSize: 22,
                      fontWeight: FontWeight.w700,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              Spacer(flex: 3),
            ],
          );
        },
      ),
    );
  }

  void onRecord(BuildContext context) async {
    if (RootService.navigator.canPop()) await RootService.navigator.pop();
    RootService.navigator.myDialog(RecordPopUp(), title: RootService.systext.getValue('didnt-like-popup.speak'));
  }

  void onWrite(BuildContext context) async {
    if (RootService.navigator.canPop()) await RootService.navigator.pop();
    RootService.appraisal.prolongAndSend();
    RootService.navigator.myDialog(SendCommentPopUp(),
        title: RootService.systext.getValue('didnt-like-popup.is-there-anything-you-didnt-like-today') +
            '\n' +
            RootService.systext.getValue('didnt-like-popup.leave-a-comment'));
  }

  void onDenied() async {
    if (RootService.navigator.canPop()) await RootService.navigator.pop();
    RootService.navigator.myDialog(ThankYouPopUp(), hasTitleAndBack: false);
    RootService.appraisal.sendRating();
    RootService.appraisal.prolongAndClose();
  }

  void onVideo(BuildContext context) async {
    if (RootService.navigator.canPop()) await RootService.navigator.pop();
    RootService.navigator.pushNamed(MyRoutes.cameraPage);
  }

  void checkMicrophonePermission(BuildContext context) async {
    RootService.appraisal.stopTimer();

    if (kIsWeb) {
      if (RootService.navigator.canPop()) await RootService.navigator.pop();
      RootService.navigator
          .myDialog(RecordForWebPopUp(), title: RootService.systext.getValue('didnt-like-popup.speak'));
    } else {
      final hasPermission = await Permission.microphone.isGranted;

      if (hasPermission) {
        onRecord(context);
      } else {
        final permissionStatus = await Permission.microphone.request();
        if (permissionStatus == PermissionStatus.granted) {
          onRecord(context);
        } else if (permissionStatus == PermissionStatus.permanentlyDenied) {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text(
                SPHelper.language == 'ru'
                    ? 'Запись отключена'
                    : SPHelper.language == 'uz'
                        ? 'Yozib olish o\'chirilgan'
                        : 'Recording disabled',
              ),
              content: Text(
                SPHelper.language == 'ru'
                    ? 'Чтобы эта функция работала, включите разрешение'
                    : SPHelper.language == 'uz'
                        ? 'Ushbu funktsiya ishlashi uchun ruxsatni yoqing'
                        : 'For this function to work, turn on permission',
              ),
              actions: [
                TextButton(
                  onPressed: () async {
                    openAppSettings();
                    if (RootService.navigator.canPop()) await RootService.navigator.pop();
                    RootService.appraisal.prolongAndSend();
                  },
                  child: Text(
                    SPHelper.language == 'ru'
                        ? 'Настройки'
                        : SPHelper.language == 'uz'
                            ? 'Sozlamalar'
                            : 'Settings',
                  ),
                ),
                TextButton(
                  onPressed: () async {
                    if (RootService.navigator.canPop(context)) await RootService.navigator.pop();
                    RootService.appraisal.prolongAndSend();
                  },
                  child: Text(
                    SPHelper.language == 'ru'
                        ? 'Отмена'
                        : SPHelper.language == 'uz'
                            ? 'Bekor qilish'
                            : 'Cancell',
                  ),
                ),
              ],
            ),
          );
        } else {
          RootService.appraisal.prolongAndSend();
        }
      }
    }
  }

  void checkCameraPermission(BuildContext context) async {
    if (kIsWeb) {
      RootService.appraisal.stopTimer();
      if (RootService.navigator.canPop()) await RootService.navigator.pop();
      RootService.navigator.pushNamed(MyRoutes.webCameraPage);
    } else {
      RootService.appraisal.stopTimer();
      final hasPermission = await Permission.camera.isGranted;

      if (hasPermission) {
        onVideo(context);
      } else {
        final permissionStatus = await Permission.camera.request();
        if (permissionStatus == PermissionStatus.granted) {
          onVideo(context);
        } else if (permissionStatus == PermissionStatus.permanentlyDenied) {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text(
                SPHelper.language == 'ru'
                    ? 'Запись отключена'
                    : SPHelper.language == 'uz'
                        ? 'Yozib olish o\'chirilgan'
                        : 'Recording disabled',
              ),
              content: Text(
                SPHelper.language == 'ru'
                    ? 'Чтобы эта функция работала, включите разрешение'
                    : SPHelper.language == 'uz'
                        ? 'Ushbu funktsiya ishlashi uchun ruxsatni yoqing'
                        : 'For this function to work, turn on permission',
              ),
              actions: [
                TextButton(
                  onPressed: () async {
                    openAppSettings();
                    if (RootService.navigator.canPop()) await RootService.navigator.pop();
                    RootService.appraisal.prolongAndSend();
                  },
                  child: Text(
                    SPHelper.language == 'ru'
                        ? 'Настройки'
                        : SPHelper.language == 'uz'
                            ? 'Sozlamalar'
                            : 'Settings',
                  ),
                ),
                TextButton(
                  onPressed: () async {
                    if (RootService.navigator.canPop(context)) await RootService.navigator.pop();
                    RootService.appraisal.prolongAndSend();
                  },
                  child: Text(
                    SPHelper.language == 'ru'
                        ? 'Отмена'
                        : SPHelper.language == 'uz'
                            ? 'Bekor qilish'
                            : 'Cancell',
                  ),
                ),
              ],
            ),
          );
        } else {
          RootService.appraisal.prolongAndSend();
        }
      }
    }
  }
}
