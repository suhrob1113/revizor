import 'package:flutter/material.dart';
import 'package:revizor/conf/assets/icon_constants.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/redux/systext/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/default/default_appbar.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_icon_asset.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/ui/language_switcher_and_logout/language_switcher_and_logout.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class ServiceIsNotActiveView extends StatelessWidget {
  final String? title;
  final String? imgUrl;

  const ServiceIsNotActiveView({Key? key, this.title, this.imgUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppbar(title ?? '', hasShadow: true, imgUrl: imgUrl),
      backgroundColor: SPHelper.themeId == 1
          ? MyColors.dark
          : SPHelper.themeId == 2
              ? MyColors.grey
              : MyColors.white,
      body: Stack(
        children: [
          Center(
            child: MyContainer(
              hasShadow: SPHelper.themeId == 2 ? true : false,
              isRounded: true,
              color: SPHelper.themeId == 1
                  ? MyColors.sailor
                  : SPHelper.themeId == 2
                      ? MyColors.white
                      : MyColors.accent,
              padding: const EdgeInsets.all(32),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: 32),
                  StreamBuilder<SystextState>(
                      stream: RootService.stores.systext.state$,
                      builder: (context, snapshot) {
                        return MyText(
                          RootService.systext.getValue('error.sorry-your-subscription-has-expired'),
                          fontSize: 37,
                          fontWeight: FontWeight.w700,
                          textAlign: TextAlign.center,
                        );
                      }),
                  SizedBox(height: 55),
                  MyIconAsset(MyIcons.smileSad, height: 130, width: 130),
                  SizedBox(height: 70),
                ],
              ),
            ),
          ),
          Positioned(
            child: LanguageSwitcherAndLogout(
              alternativeColors: true,
            ),
            bottom: 32,
            right: 32,
          ),
        ],
      ),
    );
  }
}
