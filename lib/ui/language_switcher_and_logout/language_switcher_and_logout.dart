// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/conf/assets/icon_constants.dart';
import 'package:revizor/conf/routes/main_routes_constants.dart';
import 'package:revizor/conf/sp_keys/shared_preference_constants.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_icon_asset.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class LanguageSwitcherAndLogout extends StatefulWidget {
  final bool alternativeColors;
  final bool upsidedownIcon;

  const LanguageSwitcherAndLogout({Key? key, this.alternativeColors = false, this.upsidedownIcon = false})
      : super(key: key);
  @override
  _LanguageSwitcherAndLogoutState createState() => _LanguageSwitcherAndLogoutState();
}

class _LanguageSwitcherAndLogoutState extends State<LanguageSwitcherAndLogout> {
  final items = ['RU', 'ENG', 'UZB'];

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        MyContainer(
          isRounded: true,
          padding: const EdgeInsets.only(right: 10),
          hasShadow: SPHelper.themeId == 2 ? true : false,
          color: widget.alternativeColors
              ? SPHelper.themeId == 1
                  ? MyColors.sailorLight
                  : SPHelper.themeId == 2
                      ? MyColors.white
                      : MyColors.accentDark
              : SPHelper.themeId == 1
                  ? MyColors.sailor
                  : SPHelper.themeId == 2
                      ? MyColors.white
                      : MyColors.accentDark,
          child: DropdownButtonHideUnderline(
            child: ButtonTheme(
              alignedDropdown: true,
              child: DropdownButton(
                  value: items.firstWhere((element) => element.toLowerCase().substring(0, 2) == SPHelper.language,
                      orElse: () => 'ru'),
                  dropdownColor: widget.alternativeColors
                      ? SPHelper.themeId == 1
                          ? MyColors.sailorLight
                          : SPHelper.themeId == 2
                              ? MyColors.white
                              : MyColors.accentDark
                      : SPHelper.themeId == 1
                          ? MyColors.sailor
                          : SPHelper.themeId == 2
                              ? MyColors.white
                              : MyColors.accentDark,
                  items: items
                      .map(
                        (val) => DropdownMenuItem(
                          value: val,
                          child: Row(
                            children: [
                              MyIconAsset(
                                MyIcons.flag(val),
                                size: 24,
                              ),
                              SizedBox(width: 6),
                              MyText(
                                val,
                                fontSize: 20,
                              ),
                            ],
                          ),
                        ),
                      )
                      .toList(),
                  elevation: 0,
                  icon: MyIconAsset(
                    widget.upsidedownIcon ? MyIcons.up : MyIcons.down,
                    size: 15,
                    color: SPHelper.themeId == 1
                        ? MyColors.white
                        : SPHelper.themeId == 2
                            ? MyColors.primary
                            : MyColors.white,
                  ),
                  onChanged: (val) {
                    SPHelper.language = val.toString().toLowerCase().substring(0, 2);
                    setState(() {});
                    RootService.api.create();
                    RootService.systext.fetch();
                    if (SPHelper.accessToken != null) RootService.tablet.fetch();
                  }),
            ),
          ),
        ),
        if (SPHelper.accessToken != null) ...[
          SizedBox(
            width: 16,
          ),
          MyContainer(
            onTap: () {
              SPHelper.remove(SPKeys.accessToken);
              SPHelper.remove(SPKeys.accessType);
              RootService.navigator.popUNtilFirst();
              RootService.navigator.pushReplacementNamed(MyRoutes.authenticationPage);
            },
            isRounded: true,
            height: 44,
            width: 112,
            color: SPHelper.themeId == 3 ? MyColors.white : MyColors.accent,
            child: Center(
              child: MyText(
                RootService.systext.getValue('auth-screen.logout'),
                color: SPHelper.themeId == 3 ? MyColors.accent : MyColors.white,
                fontSize: 20,
              ),
            ),
          )
        ]
      ],
    );
  }
}
