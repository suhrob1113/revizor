// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/conf/assets/icon_constants.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/theme/box_decorations.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_icon_asset.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class Stars extends StatelessWidget {
  final int starsCount;
  final bool isBig;

  const Stars({
    Key? key,
    required this.starsCount,
    this.isBig = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> starsChildren = [];
    for (var i = 0; i < starsCount; i++) {
      starsChildren.add(MyIconAsset(
        MyIcons.starOn,
        size: isBig ? 29 : 18,
      ));
      if (i != starsCount - 1) {
        starsChildren.add(SizedBox(
          width: isBig ? 1.61 : 1,
        ));
      }
    }
    if (5 - starsCount > 0) {
      for (var i = 0; i < 5 - starsCount; i++) {
        starsChildren.add(MyIconAsset(
          MyIcons.starOff,
          size: isBig ? 29 : 18,
          color: SPHelper.themeId == 1
              ? MyColors.sailor
              : SPHelper.themeId == 2
                  ? null
                  : MyColors.accent,
        ));
        if (i != 5 - starsCount - 1) {
          starsChildren.add(SizedBox(
            width: isBig ? 1.61 : 1,
          ));
        }
      }
    }
    return MyContainer(
      width: isBig ? 217 : 135,
      height: isBig ? 41 : 26,
      hasShadow: SPHelper.themeId == 2 ? true : false,
      color: SPHelper.themeId == 1
          ? MyColors.dark
          : SPHelper.themeId == 2
              ? MyColors.white
              : MyColors.accentDark,
      borderRadius: MyBorderRadius.allRounded150,
      margin: const EdgeInsets.fromLTRB(5, 5, 5, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MyContainer(
            width: isBig ? 41 : 26,
            height: isBig ? 41 : 26,
            borderRadius: const BorderRadius.horizontal(left: Radius.circular(150)),
            color: SPHelper.themeId == 1
                ? MyColors.dark
                : SPHelper.themeId == 2
                    ? MyColors.primary
                    : MyColors.accentDark,
            child: Center(
              child: MyText(
                '$starsCount',
                fontSize: isBig ? 18 : 15,
                height: 1,
                color: MyColors.white,
                textAlign: TextAlign.center,
              ),
            ),
          ),
          MyContainer(
            width: 1,
            height: isBig ? 41 : 26,
            color: SPHelper.themeId == 1
                ? MyColors.sailor
                : SPHelper.themeId == 2
                    ? MyColors.white
                    : MyColors.accent,
          ),
          Row(
            children: starsChildren,
          ),
          SizedBox(
            width: 8,
          )
        ],
      ),
    );
  }
}
