// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/theme/box_decorations.dart';
import 'package:revizor/theme/default/default_image.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/ui/employee/stars.dart';

class EmployeeItem extends StatelessWidget {
  final String name;
  final String position;
  final String image;
  final int stars;
  final bool isBig;
  final bool withStars;
  final bool isAlternative;

  const EmployeeItem({
    Key? key,
    required this.name,
    required this.position,
    required this.image,
    required this.stars,
    this.isBig = false,
    this.withStars = true,
    this.isAlternative = false,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return isAlternative
        ? Row(
            children: [
              ClipRRect(
                borderRadius: MyBorderRadius.allRounded150,
                child: MyImage(
                  image,
                  height: isBig ? 125 : 91,
                  width: isBig ? 125 : 91,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(width: 16),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                    name,
                    fontSize: isBig ? 18 : 13,
                    fontWeight: FontWeight.w700,
                  ),
                  SizedBox(height: 4),
                  MyText(
                    position,
                    fontSize: isBig ? 16 : 13,
                    fontWeight: FontWeight.w400,
                  ),
                ],
              )
            ],
          )
        : Column(
            children: [
              ClipRRect(
                borderRadius: MyBorderRadius.allRounded150,
                child: MyImage(
                  image,
                  height: isBig ? 125 : 91,
                  width: isBig ? 125 : 91,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(height: 8),
              MyText(
                name,
                fontSize: isBig ? 18 : 13,
                fontWeight: FontWeight.w700,
              ),
              SizedBox(height: 4),
              MyText(
                position,
                fontSize: isBig ? 16 : 13,
                fontWeight: FontWeight.w400,
              ),
              SizedBox(height: 6),
              if (withStars) Stars(starsCount: stars, isBig: isBig),
            ],
          );
  }
}
