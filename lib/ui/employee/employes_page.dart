// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/api/models/employee/employee.dart';
import 'package:revizor/conf/assets/icon_constants.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/conf/values/strings_constants.dart';
import 'package:revizor/redux/tablet/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/default/default_appbar.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_icon_asset.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/theme/states/loading_state.dart';
import 'package:revizor/ui/language_switcher_and_logout/language_switcher_and_logout.dart';
import 'package:revizor/ui/pop_ups/employee_pop_up.dart';
import 'package:revizor/ui/service_is_not_active_view/service_is_not_active_view.dart';
import 'package:revizor/utils/shared_preference_helper.dart';
import 'employee_item.dart';

class EmployesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: StreamBuilder<TabletState>(
        stream: RootService.stores.tablet.state$,
        builder: (context, snapshot) {
          final state = snapshot.data;
          final type = state?.tablet?.type;
          final selectedDepartment = (type == 2) ? state?.tablet?.department : state?.selectedDepartment;
          final List<Employee>? employes = selectedDepartment?.employees;
          if (state == null || selectedDepartment == null || employes == null)
            return Scaffold(
              appBar: MyAppbar(MyStrings.surgery),
              body: Stack(
                children: [
                  MyLoadingState(),
                  Positioned(
                    child: LanguageSwitcherAndLogout(
                      alternativeColors: true,
                      upsidedownIcon: true,
                    ),
                    bottom: 32,
                    right: 32,
                  ),
                ],
              ),
            );
          return (state.tablet?.status ?? true)
              ? Scaffold(
                  appBar: MyAppbar(selectedDepartment.name ?? '', imgUrl: state.tablet?.serviceInfo?.logo),
                  backgroundColor: SPHelper.themeId == 1
                      ? MyColors.dark
                      : SPHelper.themeId == 2
                          ? MyColors.grey
                          : MyColors.white,
                  body: Column(
                    children: [
                      SizedBox(height: 1),
                      MyContainer(
                        color: SPHelper.themeId == 1
                            ? MyColors.sailor
                            : SPHelper.themeId == 2
                                ? MyColors.white
                                : MyColors.accent,
                        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            if (RootService.navigator.canPop())
                              MyIconAsset(
                                MyIcons.back,
                                onTap: onBack,
                                size: 33,
                                color: SPHelper.themeId == 1
                                    ? MyColors.white
                                    : SPHelper.themeId == 2
                                        ? MyColors.primary
                                        : MyColors.white,
                              ),
                            MyText(
                              selectedDepartment.text,
                              fontSize: 24,
                              fontWeight: FontWeight.w700,
                            ),
                            LanguageSwitcherAndLogout()
                          ],
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 24),
                          child: SingleChildScrollView(
                            child: Wrap(
                                spacing: 16,
                                children: employes.map((e) {
                                  return MyContainer(
                                    onTap: () => onEmployee(context, e),
                                    padding: const EdgeInsets.fromLTRB(25, 16, 25, 10),
                                    margin: const EdgeInsets.only(bottom: 16),
                                    isRounded: true,
                                    hasShadow: SPHelper.themeId == 2 ? true : false,
                                    color: SPHelper.themeId == 1
                                        ? MyColors.sailor
                                        : SPHelper.themeId == 2
                                            ? MyColors.white
                                            : MyColors.accent,
                                    child: EmployeeItem(
                                      name: e.name!,
                                      position: e.position!,
                                      stars: e.rating!.toInt(),
                                      image: e.photo!,
                                    ),
                                  );
                                }).toList()),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              : ServiceIsNotActiveView(
                  title: state.tablet?.serviceInfo?.title,
                  imgUrl: state.tablet?.serviceInfo?.logo,
                );
        },
      ),
    );
  }

  void onEmployee(BuildContext context, Employee employee) {
    RootService.appraisal.prolongAndClose();
    RootService.tablet.selectEmployee(employee);
    RootService.navigator.myDialog(EmployeePopUp());
  }

  void onBack() {
    RootService.appraisal.prolongAndClose();
    RootService.navigator.pop();
  }
}
