// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/api/models/department/department.dart';
import 'package:revizor/conf/routes/main_routes_constants.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class DepartmentItem extends StatelessWidget {
  final Department? department;

  const DepartmentItem({Key? key, required this.department}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MyContainer(
      onTap: onTap,
      hasShadow: SPHelper.themeId == 2 ? true : false,
      color: SPHelper.themeId == 1
          ? MyColors.sailor
          : SPHelper.themeId == 2
              ? MyColors.white
              : MyColors.accent,
      padding: const EdgeInsets.all(34),
      margin: const EdgeInsets.fromLTRB(0, 0, 24, 16),
      isRounded: true,
      child: MyText(
        department?.name ?? '',
        fontSize: 32,
      ),
    );
  }

  void onTap() {
    RootService.appraisal.prolongAndClose();
    RootService.tablet.selectDepartment(department!);
    RootService.navigator.pushNamed(MyRoutes.employeesPage);
  }
}
