// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/api/models/department/department.dart';
import 'package:revizor/conf/assets/image_constants.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/conf/values/strings_constants.dart';
import 'package:revizor/redux/tablet/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/default/default_appbar.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/theme/states/loading_state.dart';
import 'package:revizor/ui/department/department_item.dart';
import 'package:revizor/ui/language_switcher_and_logout/language_switcher_and_logout.dart';
import 'package:revizor/ui/service_is_not_active_view/service_is_not_active_view.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class DepartmentsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: StreamBuilder<TabletState>(
        stream: RootService.stores.tablet.state$,
        builder: (context, snapshot) {
          final state = snapshot.data;
          final List<Department>? departments = state?.tablet?.departments ?? [];
          if (state == null || departments == null)
            return Scaffold(
              appBar: MyAppbar(MyStrings.rateOurService),
              body: Stack(
                children: [
                  MyLoadingState(),
                  Positioned(
                    child: LanguageSwitcherAndLogout(
                      alternativeColors: true,
                      upsidedownIcon: true,
                    ),
                    bottom: 32,
                    right: 32,
                  ),
                ],
              ),
            );
          return (state.tablet?.status ?? true)
              ? Scaffold(
                  appBar: MyAppbar(state.tablet?.serviceInfo?.title ?? '', imgUrl: state.tablet?.serviceInfo?.logo),
                  backgroundColor: SPHelper.themeId == 1
                      ? MyColors.dark
                      : SPHelper.themeId == 2
                          ? MyColors.grey
                          : MyColors.white,
                  body: Column(
                    children: [
                      SizedBox(height: 1),
                      MyContainer(
                        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                        color: SPHelper.themeId == 1
                            ? MyColors.sailor
                            : SPHelper.themeId == 2
                                ? MyColors.white
                                : MyColors.accent,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            StreamBuilder<Object>(
                                stream: RootService.stores.systext.state$,
                                builder: (context, snapshot) {
                                  return MyText(
                                    RootService.systext.getValue('departments-screen.select-department'),
                                    fontSize: 24,
                                  );
                                }),
                            LanguageSwitcherAndLogout(
                              alternativeColors: true,
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          children: [
                            Expanded(
                              child: MyContainer(
                                height: double.infinity,
                                isRounded: true,
                                hasShadow: SPHelper.themeId == 2 ? true : false,
                                color: SPHelper.themeId == 1
                                    ? MyColors.sailor
                                    : SPHelper.themeId == 2
                                        ? MyColors.white
                                        : MyColors.accent,
                                padding: const EdgeInsets.all(32),
                                margin: const EdgeInsets.all(24),
                                child: Image.asset(
                                  MyImages.departments,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 24),
                                child: ListView.builder(
                                  itemBuilder: (context, index) => DepartmentItem(
                                    department: departments[index],
                                  ),
                                  itemCount: departments.length,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              : ServiceIsNotActiveView(
                  title: state.tablet?.serviceInfo?.title,
                  imgUrl: state.tablet?.serviceInfo?.logo,
                );
        },
      ),
    );
  }
}
