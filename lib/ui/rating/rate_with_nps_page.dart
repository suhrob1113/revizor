import 'package:flutter/material.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/conf/values/strings_constants.dart';
import 'package:revizor/redux/tablet/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/default/default_appbar.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/theme/states/loading_state.dart';
import 'package:revizor/ui/language_switcher_and_logout/language_switcher_and_logout.dart';
import 'package:revizor/ui/pop_ups/image_full_width_pop_up.dart';
import 'package:revizor/ui/service_is_not_active_view/service_is_not_active_view.dart';
import 'package:revizor/ui/smileys/mini_smileys.dart';
import 'package:revizor/utils/media_helper.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class RateWithNpsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: StreamBuilder<TabletState>(
        stream: RootService.stores.tablet.state$,
        builder: (context, snapshot) {
          final state = snapshot.data;
          final tablet = snapshot.data?.tablet;
          final nps = tablet?.nps;
          final serviceInfo = tablet?.serviceInfo;

          if (state == null || tablet == null || nps == null)
            return Scaffold(
              appBar: MyAppbar(MyStrings.helpUsBeBetter, hasShadow: true, imgUrl: state?.tablet?.serviceInfo?.logo),
              body: Stack(
                children: [
                  MyLoadingState(),
                  Positioned(
                    child: LanguageSwitcherAndLogout(
                      alternativeColors: true,
                      upsidedownIcon: true,
                    ),
                    bottom: 32,
                    right: 32,
                  ),
                ],
              ),
            );
          return (state.tablet?.status ?? true)
              ? Scaffold(
                  appBar: MyAppbar(serviceInfo?.title ?? '', hasShadow: true, imgUrl: state.tablet?.serviceInfo?.logo),
                  backgroundColor: SPHelper.themeId == 1
                      ? MyColors.dark
                      : SPHelper.themeId == 2
                          ? MyColors.grey
                          : MyColors.white,
                  body: Stack(
                    children: [
                      MyContainer(
                        height: MediaHelper.height,
                        width: MediaHelper.width,
                        padding: const EdgeInsets.all(16),
                        color: SPHelper.themeId == 1
                            ? MyColors.dark
                            : SPHelper.themeId == 2
                                ? MyColors.grey
                                : MyColors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Spacer(
                              flex: 2,
                            ),
                            MyText(
                              nps.text ?? '',
                              fontSize: 37,
                              fontWeight: FontWeight.w700,
                              textAlign: TextAlign.center,
                            ),
                            Spacer(
                              flex: 2,
                            ),
                            MiniSmileys(
                              id: nps.id ?? 0,
                            ),
                            Spacer(
                              flex: 4,
                            )
                          ],
                        ),
                      ),
                      Positioned(
                        child: LanguageSwitcherAndLogout(
                          alternativeColors: true,
                          upsidedownIcon: true,
                        ),
                        bottom: 32,
                        right: 32,
                      ),
                    ],
                  ))
              : ServiceIsNotActiveView(
                  title: serviceInfo?.title,
                  imgUrl: serviceInfo?.logo,
                );
        },
      ),
    );
  }

  void onBanner(String imageUrl, String title) {
    RootService.appraisal.prolongAndClose();
    RootService.navigator.myDialog(ImageFullWidthPopUp(imageUrl: imageUrl), hasTitleAndBack: true, title: title);
  }
}
