// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/conf/values/strings_constants.dart';
import 'package:revizor/redux/systext/store.dart';
import 'package:revizor/redux/tablet/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/box_decorations.dart';
import 'package:revizor/theme/default/default_appbar.dart';
import 'package:revizor/theme/default/default_appbar2.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/theme/states/loading_state.dart';
import 'package:revizor/ui/employee/employee_item.dart';
import 'package:revizor/ui/language_switcher_and_logout/language_switcher_and_logout.dart';
import 'package:revizor/ui/service_is_not_active_view/service_is_not_active_view.dart';
import 'package:revizor/ui/smileys/smileys.dart';
import 'package:revizor/utils/media_helper.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class RateServicePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: StreamBuilder<TabletState>(
        stream: RootService.stores.tablet.state$,
        builder: (context, snapshot) {
          final state = snapshot.data;
          final serviceInfo = state?.tablet?.serviceInfo;
          final employee = state?.tablet?.employee;
          if (state == null || employee == null)
            return Scaffold(
              appBar: MyAppbar(
                MyStrings.helpUsBeBetter,
                imgUrl: serviceInfo?.logo,
                hasShadow: true,
              ),
              body: Stack(
                children: [
                  MyLoadingState(),
                  Positioned(
                    child: LanguageSwitcherAndLogout(
                      alternativeColors: true,
                    ),
                    top: 32,
                    right: 32,
                  ),
                ],
              ),
            );
          return (state.tablet?.status ?? true)
              ? Scaffold(
                  appBar: MyAppbar2(serviceInfo!.title ?? '', hasShadow: true, imgUrl: state.tablet?.serviceInfo?.logo),
                  backgroundColor: SPHelper.themeId == 1
                      ? MyColors.dark
                      : SPHelper.themeId == 2
                          ? MyColors.grey
                          : MyColors.white,
                  body: Stack(
                    children: [
                      Center(
                        child: MyContainer(
                          hasShadow: SPHelper.themeId == 2 ? true : false,
                          //  isRounded: true,
                          color: SPHelper.themeId == 1
                              ? MyColors.sailor
                              : SPHelper.themeId == 2
                                  ? MyColors.white
                                  : MyColors.accentDark,
                          padding: const EdgeInsets.all(32),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              //    SizedBox(height: 50),
                              Spacer(),
                              MyText(
                                employee.text ?? '',
                                fontSize: 37,
                                fontWeight: FontWeight.w700,
                                textAlign: TextAlign.center,
                              ),
                              Spacer(),
                              MyContainer(
                                hasShadow: SPHelper.themeId == 2 ? true : false,
                                isRounded: true,
                                color: SPHelper.themeId == 1
                                    ? MyColors.sailorLight
                                    : SPHelper.themeId == 2
                                        ? MyColors.white
                                        : MyColors.accent,
                                padding: const EdgeInsets.all(16),
                                child: StreamBuilder<SystextState>(
                                    stream: RootService.stores.systext.state$,
                                    builder: (context, snapshot) {
                                      return Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          EmployeeItem(
                                            name: employee.name!,
                                            position: employee.position!,
                                            stars: employee.rating!.toInt(),
                                            image: employee.photo!,
                                            isBig: MediaHelper.height >= 600 ? true : false,
                                            withStars: false,
                                            isAlternative: true,
                                          ),
                                          Spacer(),
                                          Column(
                                            children: [
                                              MyText(
                                                RootService.systext.getValue('rate-service-screen.overall-rating'),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w700,
                                                color: SPHelper.themeId == 1
                                                    ? MyColors.white
                                                    : SPHelper.themeId == 2
                                                        ? MyColors.accentDark
                                                        : MyColors.white,
                                              ),
                                              SizedBox(
                                                height: 16,
                                              ),
                                              MyText(
                                                '${employee.rating}',
                                                fontSize: 36,
                                              ),
                                            ],
                                          ),
                                          Spacer(),
                                          Column(
                                            children: [
                                              MyText(
                                                RootService.systext.getValue('rate-service-screen.number-of-voters'),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w700,
                                                color: SPHelper.themeId == 1
                                                    ? MyColors.white
                                                    : SPHelper.themeId == 2
                                                        ? MyColors.accentDark
                                                        : MyColors.white,
                                              ),
                                              SizedBox(
                                                height: 16,
                                              ),
                                              MyText(
                                                '${employee.votes}',
                                                fontSize: 36,
                                              ),
                                            ],
                                          )
                                        ],
                                      );
                                    }),
                              ),

                              Spacer(),
                              Smileys(id: employee.id!),
                              Spacer(),
                            ],
                          ),
                        ),
                      ),
                      // Positioned(
                      //   child: LanguageSwitcherAndLogout(
                      //     alternativeColors: true,
                      //   ),
                      //   top: 32,
                      //   right: 32,
                      // ),
                    ],
                  ),
                )
              : ServiceIsNotActiveView(
                  title: serviceInfo?.title,
                  imgUrl: serviceInfo?.logo,
                );
        },
      ),
    );
  }
}
