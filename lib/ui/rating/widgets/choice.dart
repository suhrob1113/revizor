// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/api/models/answer/answer.dart';
import 'package:revizor/api/models/questionnaire/questionnaire.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class Choice extends StatelessWidget {
  final Questionnaire questionnaire;

  const Choice({Key? key, required this.questionnaire}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Align(
      child: MyContainer(
        onTap: onTap,
        width: 500,
        padding: const EdgeInsets.all(30),
        margin: const EdgeInsets.only(bottom: 20),
        isRounded: true,
        hasShadow: SPHelper.themeId == 2 ? true : false,
        color: SPHelper.themeId == 1
            ? MyColors.sailor
            : SPHelper.themeId == 2
                ? MyColors.white
                : MyColors.accent,
        child: MyText(
          questionnaire.text ?? '',
          fontSize: 24,
        ),
      ),
    );
  }

  void onTap() {
    RootService.tablet.selectAnswer(questionnaire);
  }
}
