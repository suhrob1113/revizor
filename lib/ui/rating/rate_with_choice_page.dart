// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:auto_animated/auto_animated.dart';

// Project imports:
import 'package:revizor/api/models/answer/answer.dart';
import 'package:revizor/api/models/questionnaire/questionnaire.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/conf/values/strings_constants.dart';
import 'package:revizor/redux/tablet/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/default/default_appbar.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/theme/states/loading_state.dart';
import 'package:revizor/ui/language_switcher_and_logout/language_switcher_and_logout.dart';
import 'package:revizor/ui/rating/widgets/choice.dart';
import 'package:revizor/ui/service_is_not_active_view/service_is_not_active_view.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class RateWithChoicePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: StreamBuilder<TabletState>(
          stream: RootService.stores.tablet.state$,
          builder: (context, snapshot) {
            final state = snapshot.data;

            final selectedQuestinnaires = state?.selectedQuestionnaires ?? [];
            final selectedQuestinaire = state?.selectedQuestionnaire;

            if (state == null || selectedQuestinaire == null || selectedQuestinnaires.isEmpty)
              return Scaffold(
                appBar: MyAppbar(MyStrings.helpUsBeBetter, hasShadow: true, imgUrl: state?.tablet?.serviceInfo?.logo),
                body: Stack(
                  children: [
                    MyLoadingState(),
                    LanguageSwitcherAndLogout(
                      alternativeColors: true,
                    )
                  ],
                ),
              );

            final question = selectedQuestinaire.text ?? '';

            return (state.tablet?.status ?? true)
                ? Scaffold(
                    appBar: MyAppbar(state.tablet?.serviceInfo?.title ?? '', imgUrl: state.tablet?.serviceInfo?.logo),
                    backgroundColor: SPHelper.themeId == 1
                        ? MyColors.dark
                        : SPHelper.themeId == 2
                            ? MyColors.grey
                            : MyColors.white,
                    body: Column(
                      children: [
                        SizedBox(height: 1),
                        MyContainer(
                          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                          color: SPHelper.themeId == 1
                              ? MyColors.sailor
                              : SPHelper.themeId == 2
                                  ? MyColors.white
                                  : MyColors.accent,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              MyText(
                                question,
                                fontSize: 26,
                                fontWeight: FontWeight.w700,
                              ),
                              LanguageSwitcherAndLogout(
                                alternativeColors: true,
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 65),
                              child: LiveList(
                                  key: Key(selectedQuestinaire.id.toString()),
                                  addAutomaticKeepAlives: false,
                                  itemBuilder: (context, index, animation) =>
                                      buildAnimatedItem(context, selectedQuestinnaires[index], animation),
                                  itemCount: selectedQuestinnaires.length)),
                        )
                      ],
                    ),
                  )
                : ServiceIsNotActiveView(
                    title: state.tablet?.serviceInfo?.title,
                    imgUrl: state.tablet?.serviceInfo?.logo,
                  );
          }),
    );
  }

  Widget buildAnimatedItem(
    BuildContext context,
    Questionnaire questionnaire,
    Animation<double> animation,
  ) =>
      // For example wrap with fade transition
      FadeTransition(
        key: Key(questionnaire.id.toString()),
        opacity: Tween<double>(
          begin: 0,
          end: 1,
        ).animate(animation),
        // And slide transition
        child: SlideTransition(
          position: Tween<Offset>(
            begin: Offset(0, -0.1),
            end: Offset.zero,
          ).animate(animation),
          // Paste you Widget
          child: Choice(
            questionnaire: questionnaire,
          ),
        ),
      );
}
