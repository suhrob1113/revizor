// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/conf/values/strings_constants.dart';
import 'package:revizor/redux/tablet/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/box_decorations.dart';
import 'package:revizor/theme/default/default_appbar.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_image.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/theme/states/loading_state.dart';
import 'package:revizor/ui/language_switcher_and_logout/language_switcher_and_logout.dart';
import 'package:revizor/ui/pop_ups/image_full_width_pop_up.dart';
import 'package:revizor/ui/service_is_not_active_view/service_is_not_active_view.dart';
import 'package:revizor/ui/smileys/smileys.dart';
import 'package:revizor/utils/media_helper.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class RateWithBannerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: StreamBuilder<TabletState>(
        stream: RootService.stores.tablet.state$,
        builder: (context, snapshot) {
          final state = snapshot.data;
          final tablet = snapshot.data?.tablet;
          final serviceInfo = tablet?.serviceInfo;
          final department = tablet?.department;
          final type = tablet?.type;
          final title = (type == 4)
              ? (serviceInfo?.title ?? '')
              : (type == 5)
                  ? (department?.bannerTitle ?? '')
                  : '';
          final image = (type == 4)
              ? (serviceInfo?.bannerImage ?? '')
              : (type == 5)
                  ? (department?.bannerImage ?? '')
                  : '';
          final text = (type == 4)
              ? (serviceInfo?.text ?? '')
              : (type == 5)
                  ? (department?.text ?? '')
                  : '';
          final id = (type == 4)
              ? (serviceInfo?.id ?? 0)
              : (type == 5)
                  ? (department?.id ?? 0)
                  : 0;
          if (state == null || tablet == null)
            return Scaffold(
              appBar: MyAppbar(MyStrings.helpUsBeBetter, hasShadow: true, imgUrl: serviceInfo?.logo),
              body: Stack(
                children: [
                  MyLoadingState(),
                  Positioned(
                    child: LanguageSwitcherAndLogout(
                      alternativeColors: true,
                    ),
                    top: 32,
                    right: 32,
                  ),
                ],
              ),
            );
          return (state.tablet?.status ?? true)
              ? Scaffold(
                  appBar: MyAppbar(title, hasShadow: true, imgUrl: state.tablet?.serviceInfo?.logo),
                  backgroundColor: SPHelper.themeId == 1
                      ? MyColors.dark
                      : SPHelper.themeId == 2
                          ? MyColors.grey
                          : MyColors.white,
                  body: Stack(
                    children: [
                      MyContainer(
                        height: MediaHelper.height,
                        width: MediaHelper.width,
                        padding: const EdgeInsets.all(16),
                        color: SPHelper.themeId == 1
                            ? MyColors.dark
                            : SPHelper.themeId == 2
                                ? MyColors.grey
                                : MyColors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            MyContainer(
                              hasShadow: SPHelper.themeId == 2 ? true : false,
                              color: SPHelper.themeId == 1
                                  ? MyColors.sailor
                                  : SPHelper.themeId == 2
                                      ? MyColors.white
                                      : MyColors.accent,
                              isRounded: true,
                              padding: const EdgeInsets.all(32),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  MyText(
                                    text,
                                    fontSize: 37,
                                    fontWeight: FontWeight.w700,
                                    textAlign: TextAlign.center,
                                  ),
                                  SizedBox(
                                    height: MediaHelper.height > 600 ? 32 : 10,
                                  ),
                                  Smileys(
                                    id: id,
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Expanded(
                              child: GestureDetector(
                                onTap: () => onBanner(image, text),
                                child: ClipRRect(
                                    borderRadius: MyBorderRadius.allRounded15,
                                    child: MyImage(
                                      image,
                                      width: MediaHelper.width,
                                      fit: BoxFit.cover,
                                    )),
                              ),
                            )
                          ],
                        ),
                      ),
                      Positioned(
                        child: LanguageSwitcherAndLogout(
                          alternativeColors: true,
                          upsidedownIcon: true,
                        ),
                        bottom: 32,
                        right: 32,
                      ),
                    ],
                  ))
              : ServiceIsNotActiveView(
                  title: serviceInfo?.title,
                  imgUrl: serviceInfo?.logo,
                );
        },
      ),
    );
  }

  void onBanner(String imageUrl, String title) {
    RootService.appraisal.prolongAndClose();
    RootService.navigator.myDialog(ImageFullWidthPopUp(imageUrl: imageUrl), hasTitleAndBack: true, title: title);
  }
}
