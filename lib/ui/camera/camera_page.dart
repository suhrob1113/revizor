import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pausable_timer/pausable_timer.dart';
import 'package:revizor/conf/assets/image_constants.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/ui/pop_ups/send_video_pop_up.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class CameraPage extends StatefulWidget {
  @override
  _CameraPageState createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  late CameraController controller;

  PausableTimer pausableTimer = PausableTimer(Duration(minutes: 2), () {});

  bool initialized = false;

  @override
  void initState() {
    super.initState();
    initializeCamera();
  }

  void initializeCamera() async {
    final cameras = await availableCameras();
    controller = CameraController(
      cameras[1],
      ResolutionPreset.medium,
    );
//    setState(() {});
    controller.initialize().then((_) async {
      if (!mounted) {
        return;
      }
      await controller.lockCaptureOrientation(DeviceOrientation.landscapeRight);
      initialized = true;

      pausableTimer = PausableTimer(Duration(minutes: 2), () async {
        await stopAndSave();
        RootService.appraisal.sendRating();
        RootService.navigator.popUNtilFirst();
      });
      pausableTimer.start();
      controller.startVideoRecording();

      setState(() {});
    });
  }

  Future<void> stopAndSave() async {
    final xFile = await controller.stopVideoRecording();
    RootService.appraisal.updateVideoPath(xFile.path);
    return;
  }

  @override
  void dispose() {
    controller.dispose();
    pausableTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!initialized) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    return Stack(
      alignment: Alignment.center,
      children: [
        CameraPreview(controller),
        Positioned(
          right: 32,
          child: GestureDetector(
            onTap: () => onPause(context),
            child: Image.asset(
              SPHelper.themeId == 3 ? MyImages.pauseWhite : MyImages.pause,
              width: 64,
              height: 64,
            ),
          ),
        ),
      ],
    );
  }

  void onPause(BuildContext context) async {
    await stopAndSave();
    pausableTimer.cancel();
    await RootService.navigator.pop();
    RootService.navigator.myDialog(SendVideoPopUp(),
        title: RootService.systext.getValue('didnt-like-popup.is-there-anything-you-didnt-like-today') +
            '\n' +
            RootService.systext.getValue('didnt-like-popup.leave-a-video-recording'));
  }
}
