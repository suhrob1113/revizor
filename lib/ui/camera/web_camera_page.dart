// ignore: avoid_web_libr

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:pausable_timer/pausable_timer.dart';

// Project imports:
import 'package:revizor/conf/assets/image_constants.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/ui/pop_ups/send_video_pop_up.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class WebcameraPage extends StatefulWidget {
  @override
  _WebcameraPageState createState() => _WebcameraPageState();
}

class _WebcameraPageState extends State<WebcameraPage> {
  MediaStream? _localStream;
  final _localRenderer = RTCVideoRenderer();
  MediaRecorder? _mediaRecorder;

  late PausableTimer pausableTimer;

  @override
  void initState() {
    super.initState();
    initRenderers();
  }

  @override
  void deactivate() {
    super.deactivate();
    _localRenderer.dispose();
    pausableTimer.cancel();
  }

  void initRenderers() async {
    await _localRenderer.initialize();
    _makeCall();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  void _makeCall() async {
    final mediaConstraints = <String, dynamic>{
      'audio': true,
      'video': {
        'mandatory': {
          'maxWidth': '1024', // Provide your own width, height and frame rate here
          'maxHeight': '768',
          'maxFrameRate': '30',
        },
        'facingMode': 'user',
        'optional': [],
      }
    };

    try {
      var stream = await navigator.mediaDevices.getUserMedia(mediaConstraints);
      _localStream = stream;
      _localRenderer.srcObject = _localStream;
      _startRecording();
    } catch (e) {
      print(e.toString());
    }
    if (!mounted) return;
  }

  void _startRecording() async {
    if (_localStream == null) throw Exception('Stream is not initialized');

    _mediaRecorder = MediaRecorder();

    setState(() {});
    _mediaRecorder!.startWeb(_localStream!);
    pausableTimer = PausableTimer(Duration(minutes: 2), () {
      _mediaRecorder!.stop().then((value) {
        RootService.appraisal.updateVideoPath(value);
        RootService.appraisal.sendRating();
        RootService.navigator.popUNtilFirst();
      });
    });
    pausableTimer.start();
  }

  void _stopRecording() async {
    pausableTimer.cancel();
    await RootService.navigator.pop();
    _mediaRecorder!.stop().then((value) {
      RootService.appraisal.updateVideoPath(value);
    });
    _mediaRecorder = null;
    RootService.navigator.myDialog(SendVideoPopUp(),
        title: RootService.systext.getValue('didnt-like-popup.is-there-anything-you-didnt-like-today') +
            '\n' +
            RootService.systext.getValue('didnt-like-popup.leave-a-video-recording'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.black,
      body: Stack(
        alignment: Alignment.center,
        children: [
          OrientationBuilder(
            builder: (context, orientation) {
              return Center(
                child: Container(
                  margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(color: Colors.black54),
                  child: RTCVideoView(_localRenderer, mirror: true),
                ),
              );
            },
          ),
          Positioned(
            right: 32,
            child: GestureDetector(
              onTap: _stopRecording,
              child: Image.asset(
                SPHelper.themeId == 3 ? MyImages.pauseWhite : MyImages.pause,
                width: 64,
                height: 64,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
