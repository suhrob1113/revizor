// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/conf/assets/icon_constants.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/redux/authentication/store.dart';
import 'package:revizor/redux/systext/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/default/default_authentication_text_field.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_icon_asset.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/ui/language_switcher_and_logout/language_switcher_and_logout.dart';
import 'package:revizor/utils/media_helper.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class AuthenticationPage extends StatefulWidget {
  @override
  _AuthenticationPageState createState() => _AuthenticationPageState();
}

class _AuthenticationPageState extends State<AuthenticationPage> {
  late TextEditingController emailTextController;

  late TextEditingController passwordTextController;

  late FocusNode emailFocusNode;

  late FocusNode passwordFocusNode;

  bool emailFieldActive = false;
  bool passwrodFieldActive = false;

  @override
  void initState() {
    emailTextController = TextEditingController();
    passwordTextController = TextEditingController();
    emailFocusNode = FocusNode();
    passwordFocusNode = FocusNode();
    emailFocusNode.addListener(() {
      emailFieldActive = emailFocusNode.hasFocus;
      updateState();
    });
    passwordFocusNode.addListener(() {
      passwrodFieldActive = passwordFocusNode.hasFocus;
      updateState();
    });

    super.initState();
  }

  @override
  void dispose() async {
    emailTextController.dispose();
    passwordTextController.dispose();
    emailFocusNode.dispose();
    passwordFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          body: Stack(
            alignment: Alignment.bottomRight,
            children: [
              MyContainer(
                height: MediaHelper.height,
                width: MediaHelper.width,
                padding: const EdgeInsets.all(32),
                color: SPHelper.themeId == 1
                    ? MyColors.dark
                    : SPHelper.themeId == 2
                        ? MyColors.white
                        : MyColors.white,
                child: Center(
                  child: SingleChildScrollView(
                    child: Column(
                      //   shrinkWrap: true,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        MyIconAsset(
                          MyIcons.logo,
                          width: 308,
                          height: 95,
                          color: SPHelper.themeId == 1 ? MyColors.white : null,
                        ),
                        SizedBox(
                          height: 45,
                        ),
                        MyContainer(
                          isRounded: true,
                          padding: const EdgeInsets.symmetric(
                            horizontal: 54,
                            vertical: 32,
                          ),
                          hasShadow: SPHelper.themeId == 1 ? false : true,
                          color: SPHelper.themeId == 1
                              ? MyColors.sailor
                              : SPHelper.themeId == 2
                                  ? MyColors.white
                                  : MyColors.accent,
                          width: 430,
                          child: StreamBuilder<SystextState>(
                              stream: RootService.stores.systext.state$,
                              builder: (context, snapshot) {
                                return Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    MyText(
                                      RootService.systext.getValue('auth-screen.welcome'),
                                      // MyStrings.welcome,
                                      fontSize: 26,
                                      height: 1,
                                    ),
                                    SizedBox(
                                      height: 50,
                                    ),
                                    MyAuthenticationTextField(
                                      controller: emailTextController,
                                      focus: emailFocusNode,
                                      onChanged: RootService.authentication.updateUsername,
                                      onFieldSubmitted: (_) {
                                        FocusScope.of(context).requestFocus(passwordFocusNode);
                                      },
                                      hintText: RootService.systext.getValue('auth-screen.username'),
                                      textCapitalization: TextCapitalization.none,
                                      prefixIcon: SizedBox(
                                        width: 25,
                                        height: 25,
                                        child: MyIconAsset(
                                          MyIcons.email,
                                          size: 25,
                                          color: emailFieldActive
                                              ? SPHelper.themeId == 1
                                                  ? MyColors.white
                                                  : SPHelper.themeId == 2
                                                      ? MyColors.primary
                                                      : MyColors.white
                                              : SPHelper.themeId == 1
                                                  ? MyColors.grey30
                                                  : SPHelper.themeId == 2
                                                      ? MyColors.primary35
                                                      : MyColors.white30,
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 20),
                                    MyAuthenticationTextField(
                                      controller: passwordTextController,
                                      onChanged: RootService.authentication.updatePassword,
                                      onFieldSubmitted: (_) => onEnter(),
                                      focus: passwordFocusNode,
                                      textCapitalization: TextCapitalization.none,
                                      obscureText: true,
                                      hintText: RootService.systext.getValue('auth-screen.password'),
                                      prefixIcon: SizedBox(
                                        width: 25,
                                        height: 25,
                                        child: MyIconAsset(
                                          MyIcons.password,
                                          size: 25,
                                          color: passwrodFieldActive
                                              ? SPHelper.themeId == 1
                                                  ? MyColors.white
                                                  : SPHelper.themeId == 2
                                                      ? MyColors.primary
                                                      : MyColors.white
                                              : SPHelper.themeId == 1
                                                  ? MyColors.grey30
                                                  : SPHelper.themeId == 2
                                                      ? MyColors.primary35
                                                      : MyColors.white30,
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 40),
                                    StreamBuilder<AuthenticationState>(
                                      stream: RootService.stores.authentication.state$,
                                      builder: (context, snapshot) => (snapshot.data?.isLoading ?? false)
                                          ? CircularProgressIndicator()
                                          : MyText(
                                              RootService.systext.getValue('auth-screen.enter'),
                                              onTap: onEnter,
                                              fontSize: 24,
                                              fontWeight: FontWeight.w700,
                                            ),
                                    )
                                  ],
                                );
                              }),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                child: LanguageSwitcherAndLogout(),
                top: 32,
                right: 32,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onCloseAlert() {
    RootService.navigator.pop();
  }

  void updateState() => {if (mounted) setState(() {})};

  void onEnter() {
    if (emailTextController.text.isNotEmpty && passwordTextController.text.isNotEmpty)
      RootService.authentication.authenticate();

    // if (emailTextController?.text == '1')
    //   RootService.navigator.pushReplacementNamed(MyRoutes.departmentsPage);
    // else if (emailTextController?.text == '2')
    //   RootService.navigator.pushReplacementNamed(MyRoutes.rateServicePage);
    // else if (emailTextController?.text == '3')
    //   RootService.navigator.pushReplacementNamed(MyRoutes.rateWithBarner);
    // else if (emailTextController?.text == '4')
    //   RootService.navigator.pushReplacementNamed(MyRoutes.rateWithChoice);

    // if (passwordTextController?.text == '1') {
    //   SPHelper.themeId = 1;
    // } else if (passwordTextController?.text == '2') {
    //   SPHelper.themeId = 2;
    // } else if (passwordTextController?.text == '3') {
    //   SPHelper.themeId = 3;
    // }
  }
}
