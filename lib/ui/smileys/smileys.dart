// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/api/models/emoji/emoji.dart';
import 'package:revizor/redux/tablet/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/states/loading_state.dart';
import 'package:revizor/ui/smileys/smiley.dart';

class Smileys extends StatelessWidget {
  final int id;

  const Smileys({Key? key, required this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    List<Widget> children = [];

    return StreamBuilder<TabletState>(
      stream: RootService.stores.tablet.state$,
      builder: (context, snapshot) {
        final state = snapshot.data;
        final List<Emoji>? emojis = state?.tablet?.emojis?.items;

        if (state == null || emojis == null) return MyLoadingState();

        children.add(Spacer(
          flex: 2,
        ));
        for (var i = 0; i < emojis.length; i++) {
          children.add(Smiley(
            emoji: emojis[i],
            id: id,
          ));
          if (i != 5) {
            children.add(Spacer());
          }
        }
        children.add(Spacer(
          flex: 2,
        ));

        return Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: children,
        );
      },
    );
  }
}
