// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/api/models/emoji/emoji.dart';
import 'package:revizor/api/models/nps/nps.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/redux/tablet/store.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/theme/states/loading_state.dart';
import 'package:revizor/ui/smileys/smiley.dart';

class MiniSmileys extends StatelessWidget {
  final int id;

  const MiniSmileys({Key? key, required this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    List<Widget> children = [];

    return StreamBuilder<TabletState>(
      stream: RootService.stores.tablet.state$,
      builder: (context, snapshot) {
        final state = snapshot.data;
        final Nps? nps = state?.tablet?.nps;
        final List<Emoji>? emojis = nps?.npsEmojis?.emojis ?? [];

        if (state == null || emojis == null) return MyLoadingState();

        children.add(Spacer(
          flex: 2,
        ));
        for (var i = 0; i < emojis.length; i++) {
          children.add(Smiley(
            emoji: emojis[i],
            id: id,
            isMini: true,
          ));
          if (i < 9) {
            children.add(Spacer());
          }
        }
        children.add(Spacer(
          flex: 2,
        ));

        return SizedBox(
          child: Column(
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: children,
              ),
              SizedBox(height: 16),
              SizedBox(
                child: Row(
                  children: [
                    Spacer(
                      flex: 1,
                    ),
                    Expanded(
                      flex: 12,
                      child: MyText(
                        nps?.npsEmojis?.textDetractors ?? '',
                        textAlign: TextAlign.center,
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        color: MyColors.red,
                      ),
                    ),
                    Expanded(
                      flex: 4,
                      child: MyText(
                        nps?.npsEmojis?.textPassives ?? '',
                        textAlign: TextAlign.center,
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        color: MyColors.yellowDarker,
                      ),
                    ),
                    Expanded(
                      flex: 4,
                      child: MyText(
                        nps?.npsEmojis?.textPromoters ?? '',
                        textAlign: TextAlign.center,
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        color: MyColors.green,
                      ),
                    ),
                    Spacer(
                      flex: 1,
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
