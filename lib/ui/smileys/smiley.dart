// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/api/models/emoji/emoji.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/box_decorations.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_image.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class Smiley extends StatefulWidget {
  //final int rating;
  final int id;
  final Emoji emoji;
  final bool isMini;

  const Smiley({Key? key, required this.emoji, required this.id, this.isMini = false}) : super(key: key);

  @override
  _SmileyState createState() => _SmileyState();
}

class _SmileyState extends State<Smiley> with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;

  bool canTap = true;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(duration: const Duration(seconds: 1), vsync: this);
    _animation = Tween<double>(begin: 1, end: 1.47).animate(_controller);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (widget.isMini) ...[
          SizedBox(
            width: 70,
            child: MyText(
              widget.emoji.appraisal.toString(),
              textAlign: TextAlign.center,
              fontSize: 22,
              fontWeight: FontWeight.w700,
              color: ((widget.emoji.appraisal ?? 0) < 7)
                  ? MyColors.red
                  : ((widget.emoji.appraisal ?? 0) < 9)
                      ? MyColors.yellowDarker
                      : MyColors.green,
            ),
          ),
          SizedBox(height: 16),
        ],
        Stack(
          alignment: Alignment.center,
          children: [
            MyContainer(
              //  padding: const EdgeInsets.all(23),
              borderRadius: MyBorderRadius.allRounded150,
              color: SPHelper.themeId == 1
                  ? MyColors.sailorLight
                  : SPHelper.themeId == 2
                      ? MyColors.white
                      : MyColors.accentDark,
              hasShadow: SPHelper.themeId == 2 ? true : false,
              width: widget.isMini ? 70 : 140,
              height: widget.isMini ? 70 : 140,
            ),
            GestureDetector(
              onTap: onTap,
              child: AnimatedSize(
                duration: Duration(seconds: 1),
                curve: Curves.easeIn,
                vsync: this,
                child: ScaleTransition(
                  scale: _animation,
                  child: ClipRRect(
                    borderRadius: MyBorderRadius.allRounded150,
                    child: MyImage(
                      widget.emoji.emoji ?? '',
                      width: widget.isMini ? 48 : 95,
                      height: widget.isMini ? 48 : 95,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        if (!widget.isMini) ...[
          SizedBox(height: 16),
          SizedBox(
            width: 140,
            child: MyText(
              widget.emoji.text,
              textAlign: TextAlign.center,
              fontSize: 24,
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      ],
    );
  }

  void onTap() async {
    if (canTap) {
      canTap = false;
      _controller.forward();
      await Future.delayed(Duration(seconds: 1));
      if (RootService.navigator.canPop()) await RootService.navigator.pop();
      RootService.appraisal.updateRating(widget.emoji.appraisal!, widget.id, widget.isMini);
      _controller.reset();
      canTap = true;
    }
  }
}
