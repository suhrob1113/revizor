// Project imports:
import 'package:revizor/api/models/answer/answer.dart';
import 'package:revizor/api/models/department/department.dart';
import 'package:revizor/api/models/employee/employee.dart';
import 'package:revizor/api/models/questionnaire/questionnaire.dart';
import 'package:revizor/api/models/service_info/service_info.dart';
import 'package:revizor/api/models/tablet/tablet.dart';

class FetchTabletAction {}

class UpdateTabletAction {
  final Tablet tablet;

  UpdateTabletAction(this.tablet);
}

class SelectDepartmentAction {
  final Department department;

  SelectDepartmentAction(this.department);
}

class SelectEmployeeAction {
  final Employee employee;

  SelectEmployeeAction(this.employee);
}

class SelectServiceInfoAction {
  final ServiceInfo serviceInfo;

  SelectServiceInfoAction(this.serviceInfo);
}

class SelectQuestionnaireAction {
  final Questionnaire? questionnaire;

  SelectQuestionnaireAction(this.questionnaire);
}

class UpdateSelectQuestionnaireAction {
  final Questionnaire? questionnaire;

  UpdateSelectQuestionnaireAction(this.questionnaire);
}

class UpdateSelectedQuestionnairesAction {
  final List<Questionnaire> questionnaires;

  UpdateSelectedQuestionnairesAction(this.questionnaires);
}

class UpdateQuestionnairesAction {
  final List<Questionnaire> questionnaires;

  UpdateQuestionnairesAction(this.questionnaires);
}

class TabletErrorAction {
  final String errorMessage;

  TabletErrorAction(this.errorMessage);
}
