// Flutter imports:
import 'package:flutter/foundation.dart';

// Project imports:
import 'package:revizor/redux/tablet/actions.dart';
import 'store.dart';

TabletState tabletReducer(TabletState state, action) {
  debugPrint('tabletReducer: New Action $action');

  if (action is UpdateTabletAction) {
    return TabletState(
      tablet: action.tablet,
      selectedDepartment: state.selectedDepartment,
      selectedEmployee: state.selectedEmployee,
      selectedQuestionnaire: state.selectedQuestionnaire,
      selectedServiceInfo: state.selectedServiceInfo,
      selectedQuestionnaires: state.selectedQuestionnaires,
      questionnaires: state.questionnaires,
      action: action,
    );
  }

  if (action is SelectDepartmentAction) {
    return TabletState(
      tablet: state.tablet,
      selectedDepartment: action.department,
      selectedEmployee: state.selectedEmployee,
      selectedQuestionnaire: state.selectedQuestionnaire,
      selectedServiceInfo: state.selectedServiceInfo,
      selectedQuestionnaires: state.selectedQuestionnaires,
      questionnaires: state.questionnaires,
      action: action,
      errorMessage: state.errorMessage,
    );
  }

  if (action is SelectEmployeeAction) {
    return TabletState(
      tablet: state.tablet,
      selectedDepartment: state.selectedDepartment,
      selectedEmployee: action.employee,
      selectedQuestionnaire: state.selectedQuestionnaire,
      selectedServiceInfo: state.selectedServiceInfo,
      selectedQuestionnaires: state.selectedQuestionnaires,
      questionnaires: state.questionnaires,
      action: action,
      errorMessage: state.errorMessage,
    );
  }

  if (action is SelectServiceInfoAction) {
    return TabletState(
      tablet: state.tablet,
      selectedDepartment: state.selectedDepartment,
      selectedEmployee: state.selectedEmployee,
      selectedQuestionnaire: state.selectedQuestionnaire,
      selectedServiceInfo: action.serviceInfo,
      selectedQuestionnaires: state.selectedQuestionnaires,
      questionnaires: state.questionnaires,
      action: action,
      errorMessage: state.errorMessage,
    );
  }

  if (action is UpdateSelectQuestionnaireAction) {
    return TabletState(
      tablet: state.tablet,
      selectedDepartment: state.selectedDepartment,
      selectedEmployee: state.selectedEmployee,
      selectedQuestionnaire: action.questionnaire,
      selectedServiceInfo: state.selectedServiceInfo,
      selectedQuestionnaires: state.selectedQuestionnaires,
      questionnaires: state.questionnaires,
      action: action,
      errorMessage: state.errorMessage,
    );
  }

  if (action is UpdateSelectedQuestionnairesAction) {
    return TabletState(
      tablet: state.tablet,
      selectedDepartment: state.selectedDepartment,
      selectedEmployee: state.selectedEmployee,
      selectedQuestionnaire: state.selectedQuestionnaire,
      selectedServiceInfo: state.selectedServiceInfo,
      selectedQuestionnaires: action.questionnaires,
      questionnaires: state.questionnaires,
      action: action,
      errorMessage: state.errorMessage,
    );
  }

  if (action is UpdateQuestionnairesAction) {
    return TabletState(
      tablet: state.tablet,
      selectedDepartment: state.selectedDepartment,
      selectedEmployee: state.selectedEmployee,
      selectedQuestionnaire: state.selectedQuestionnaire,
      selectedServiceInfo: state.selectedServiceInfo,
      selectedQuestionnaires: state.selectedQuestionnaires,
      questionnaires: action.questionnaires,
      action: action,
      errorMessage: state.errorMessage,
    );
  }

  if (action is TabletErrorAction) {
    return TabletState(
      tablet: state.tablet,
      selectedDepartment: state.selectedDepartment,
      selectedEmployee: state.selectedEmployee,
      selectedQuestionnaire: state.selectedQuestionnaire,
      selectedServiceInfo: state.selectedServiceInfo,
      selectedQuestionnaires: state.selectedQuestionnaires,
      questionnaires: state.questionnaires,
      action: action,
      errorMessage: action.errorMessage,
    );
  }

  return state;
}
