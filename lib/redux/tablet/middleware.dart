// Flutter imports:
import 'package:flutter/foundation.dart';

// Package imports:
import 'package:redux/redux.dart';

// Project imports:
import 'package:revizor/api/api_helper.dart';
import 'package:revizor/api/models/answer/answer.dart';
import 'package:revizor/api/models/questionnaire/questionnaire.dart';
import 'package:revizor/redux/tablet/actions.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/ui/pop_ups/thank_you_pop_up.dart';
import 'package:revizor/utils/shared_preference_helper.dart';
import 'store.dart';

void tabletMiddleware(Store<TabletState> store, action, NextDispatcher next) async {
  debugPrint('tabletMiddleware: New Action ${action.toString()}');

  if (action is FetchTabletAction) {
    final result = await RootService.api.tabletApi.fetch();

    if (result.isSuccessful) {
      SPHelper.themeId = result.body?.theme ?? 2;
      store.dispatch(UpdateTabletAction(result.body!));
      RootService.beginSocketService(result.body?.serviceInfo?.id ?? 0);
      if (result.body!.questionnaire != null) {
        store.dispatch(UpdateQuestionnairesAction(result.body!.questionnaire!));
        final rootQuestionnaire = result.body!.questionnaire!.firstWhere((q) => q.parent == null && q.messageType == 1);
        store.dispatch(UpdateSelectQuestionnaireAction(rootQuestionnaire));
        final List<Questionnaire> questionnaires =
            result.body!.questionnaire!.where((q) => q.parent == rootQuestionnaire.id && q.messageType == 2).toList();
        store.dispatch(UpdateSelectedQuestionnairesAction(questionnaires));
      }

      final list = store.state.tablet?.departments;
      final selectedDepartment = store.state.selectedDepartment;
      final selectedEmployee = store.state.selectedEmployee;
      if (list != null && list.isNotEmpty) {
        if (selectedDepartment != null) {
          final newSelectedDepartment = list.firstWhere(
            (element) => element.id == selectedDepartment.id,
          );
          store.dispatch(SelectDepartmentAction(newSelectedDepartment));
        }
        if (selectedDepartment != null) {
          if (selectedEmployee != null) {
            final newSelectedEmployee = selectedDepartment.employees?.firstWhere(
              (element) => element.id == selectedEmployee.id,
            );
            if (newSelectedEmployee != null) {
              store.dispatch(SelectEmployeeAction(newSelectedEmployee));
            }
          }
        }
      }
    } else {
      store.dispatch(TabletErrorAction(await getMsgFromError(result)));
      print(await getMsgFromError(result));
    }
  }

  if (action is SelectQuestionnaireAction) {
    Questionnaire selectedQuestinaire = store.state.questionnaires!.firstWhere(
        (q) => q.parent == action.questionnaire!.id && q.messageType == 1,
        orElse: () => Questionnaire(id: -1));
    if (selectedQuestinaire.id != -1) {
      List<Questionnaire> selectedQuestinaires =
          store.state.questionnaires!.where((q) => q.parent == selectedQuestinaire.id && q.messageType == 2).toList();

      store.dispatch(UpdateSelectQuestionnaireAction(selectedQuestinaire));
      store.dispatch(UpdateSelectedQuestionnairesAction(selectedQuestinaires));

      RootService.appraisal.updateAnswer(action.questionnaire!.id!);
      RootService.appraisal.prolongAndSendAnswer();
    } else {
      RootService.appraisal.updateAnswer(action.questionnaire!.id!);
      RootService.appraisal.sendRating();
      RootService.navigator.myDialog(ThankYouPopUp(), hasTitleAndBack: false);
      RootService.appraisal.prolongAndClose();
    }
  }
  next(action);
}
