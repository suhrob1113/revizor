// Project imports:
import 'package:revizor/api/models/answer/answer.dart';
import 'package:revizor/api/models/department/department.dart';
import 'package:revizor/api/models/employee/employee.dart';
import 'package:revizor/api/models/question/question.dart';
import 'package:revizor/api/models/questionnaire/questionnaire.dart';
import 'package:revizor/api/models/service_info/service_info.dart';
import 'package:revizor/api/models/tablet/tablet.dart';
import '../observable_store.dart';
import 'middleware.dart';
import 'reducers.dart';

class TabletState extends ObservableState {
  final Tablet? tablet;
  final Department? selectedDepartment;
  final Employee? selectedEmployee;
  final ServiceInfo? selectedServiceInfo;
  final Questionnaire? selectedQuestionnaire;
  final List<Questionnaire>? selectedQuestionnaires;
  final List<Questionnaire>? questionnaires;

  final action;
  TabletState({
    required this.tablet,
    required this.selectedDepartment,
    required this.selectedEmployee,
    required this.selectedServiceInfo,
    required this.selectedQuestionnaire,
    required this.selectedQuestionnaires,
    required this.questionnaires,
    required this.action,
    isLoading = false,
    errorMessage,
  }) : super(
          isLoading: isLoading,
          errorMessage: errorMessage,
        );
  factory TabletState.init() => TabletState(
        tablet: null,
        selectedDepartment: null,
        selectedEmployee: null,
        selectedServiceInfo: null,
        selectedQuestionnaire: null,
        selectedQuestionnaires: [],
        questionnaires: [],
        action: null,
      );
}

class TabletStore extends ObservableStore<TabletState, dynamic> {
  TabletStore()
      : super(
          tabletReducer,
          TabletState.init(),
          [tabletMiddleware],
        );
}
