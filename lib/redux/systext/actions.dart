// Project imports:
import 'package:revizor/api/models/systext/systext.dart';

class FetchSystextsAction {}

class UpdateSystextsAction {
  final List<Systext> systexts;

  UpdateSystextsAction(this.systexts);
}

class SystextErrorAction {
  final String errorMessage;

  SystextErrorAction(this.errorMessage);
}
