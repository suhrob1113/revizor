// Flutter imports:
import 'package:flutter/foundation.dart';

// Package imports:
import 'package:redux/redux.dart';

// Project imports:
import 'package:revizor/api/api_helper.dart';
import 'package:revizor/services/root_service.dart';
import 'actions.dart';
import 'store.dart';

void systextMiddleware(Store<SystextState> store, action, NextDispatcher next) async {
  debugPrint('systextMiddleware: New Action ${action.toString()}');

  if (action is FetchSystextsAction) {
    final result = await RootService.api.systextApi.getTexts();

    if (result.isSuccessful) {
      store.dispatch(UpdateSystextsAction(result.body!));
    } else {
      store.dispatch(SystextErrorAction(await getMsgFromError(result)));
    }
  }
  next(action);
}
