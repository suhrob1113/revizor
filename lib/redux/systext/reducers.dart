// Flutter imports:
import 'package:flutter/foundation.dart';

// Project imports:
import 'actions.dart';
import 'store.dart';

SystextState systextReducer(SystextState state, action) {
  debugPrint('systextReducer: New Action $action');

  if (action is UpdateSystextsAction) {
    return SystextState(
      systexts: action.systexts,
      action: action,
    );
  }
  return state;
}
