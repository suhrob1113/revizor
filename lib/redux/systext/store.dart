// Project imports:
import 'package:revizor/api/models/systext/systext.dart';
import '../observable_store.dart';
import 'middleware.dart';
import 'reducers.dart';

class SystextState extends ObservableState {
  final List<Systext>? systexts;

  final action;
  SystextState({
    required this.systexts,
    required this.action,
    isLoading = false,
    errorMessage,
  }) : super(
          isLoading: isLoading,
          errorMessage: errorMessage,
        );
  factory SystextState.init() => SystextState(
        systexts: [],
        action: null,
      );
}

class SystextStore extends ObservableStore<SystextState, dynamic> {
  SystextStore()
      : super(
          systextReducer,
          SystextState.init(),
          [systextMiddleware],
        );
}
