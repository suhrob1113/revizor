// Package imports:
import 'package:redux/redux.dart';
import 'package:rxdart/rxdart.dart';

// Project imports:

class ObservableState {
  final bool? isLoading;
  final String? errorMessage;

  ObservableState({
    this.isLoading = false,
    this.errorMessage,
  });
}

class ObservableStore<StateType extends ObservableState, ActionType> extends Store<StateType> {
  final BehaviorSubject<StateType> _stream = BehaviorSubject<StateType>();

  ObservableStore(
    StateType reducer(StateType prev, action),
    StateType initialState,
    middleware,
  ) : super(
          reducer,
          initialState: initialState,
          middleware: middleware,
        ) {
    onChange.pipe(_stream);
  }

  /// Наблюдаемое состояние хранилища
  BehaviorSubject<StateType> get state$ => _stream;

  dispose() {
    _stream.close();
  }
}
