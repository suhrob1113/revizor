// Project imports:
import 'package:revizor/api/models/appraisal/appraisal.dart';
import '../observable_store.dart';
import 'middleware.dart';
import 'reducers.dart';

class AppraisalState extends ObservableState {
  Appraisal? appraisal;
  final action;

  AppraisalState({
    required this.appraisal,
    required this.action,
    isLoading = false,
    errorMessage,
  }) : super(
          isLoading: isLoading,
          errorMessage: errorMessage,
        );
  factory AppraisalState.init() => AppraisalState(
        appraisal: null,
        action: null,
      );
}

class AppraisalStore extends ObservableStore<AppraisalState, dynamic> {
  AppraisalStore()
      : super(
          appraisalReducer,
          AppraisalState.init(),
          [appraisalMiddleware],
        );
}
