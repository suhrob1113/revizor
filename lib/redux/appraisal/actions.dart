class UpdateRatingAction {
  final int rating;
  final int id;

  UpdateRatingAction(this.rating, this.id);
}

class UpdateAnswerAction {
  final int id;

  UpdateAnswerAction(this.id);
}

class UpdatePhoneNumberAction {
  final String phoneNumber;

  UpdatePhoneNumberAction(this.phoneNumber);
}

class UpdateAudioPathAction {
  final String audioPath;

  UpdateAudioPathAction(this.audioPath);
}

class UpdateVideoPathAction {
  final String videoPath;

  UpdateVideoPathAction(this.videoPath);
}

class UpdateCommentAction {
  final String comment;

  UpdateCommentAction(this.comment);
}

class AppraisalErrorAction {
  final String errorMessage;

  AppraisalErrorAction(this.errorMessage);
}

class SendRatingAction {}
