import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:revizor/api/services/appraisal/custom_appraisal_api.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/utils/connectivity_helper.dart';

import 'package:revizor/utils/shared_preference_helper.dart';

import 'actions.dart';
import 'store.dart';

void appraisalMiddleware(Store<AppraisalState> store, action, NextDispatcher next) async {
  debugPrint('appraisalMiddleware: New Action ${action.toString()}');

  if (action is SendRatingAction) {
    final state = store.state;

    if (state.appraisal != null &&
        (state.appraisal!.questionnaire != null ||
            state.appraisal!.nps != null ||
            ((state.appraisal!.department != null ||
                    state.appraisal!.service != null ||
                    state.appraisal!.employee != null) &&
                state.appraisal!.star != null))) {
      if (await CustomAppraisalApi.customAppraisalRequest(
        state.appraisal!,
        isWeb: kIsWeb,
      )) {
        if (state.appraisal!.audioPath != null && !kIsWeb) {
          final file = File(state.appraisal!.audioPath!);
          await file.delete();
        }
        if (state.appraisal!.videoPath != null && !kIsWeb) {
          final file = File(state.appraisal!.videoPath!);
          await file.delete();
        }
        RootService.tablet.fetch();
      } else {
        store.dispatch(AppraisalErrorAction('error'));
        if (!await ConnectivityHelper.isConnected() && !kIsWeb) {
          List<String> unsuccessfulAppraisalList = SPHelper.unsuccessfulAppraisalList;
          if (store.state.appraisal != null) {
            unsuccessfulAppraisalList.add(json.encode(store.state.appraisal!.toJson()));
            SPHelper.unsuccessfulAppraisalList = unsuccessfulAppraisalList;
          }
        }
      }
    }
  }

  next(action);
}
