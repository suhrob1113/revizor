// Flutter imports:
import 'package:flutter/foundation.dart';

// Project imports:
import 'package:revizor/api/models/appraisal/appraisal.dart';
import 'package:revizor/redux/appraisal/actions.dart';
import 'package:revizor/services/root_service.dart';
import 'store.dart';

AppraisalState appraisalReducer(AppraisalState state, action) {
  debugPrint('appraisalReducer: New Action $action');

  if (action is UpdateRatingAction) {
    final type = RootService.stores.tablet.state.tablet!.type;
    Appraisal appraisal = Appraisal(star: action.rating);
    if (type == 1 || type == 2 || type == 3) appraisal = appraisal.copyWith(employee: action.id);
    if (type == 4) appraisal = appraisal.copyWith(service: action.id);
    if (type == 5) appraisal = appraisal.copyWith(department: action.id);
    if (type == 7) appraisal = appraisal.copyWith(nps: action.id);

    return AppraisalState(
      appraisal: appraisal,
      action: action,
    );
  }

  if (action is UpdatePhoneNumberAction) {
    return AppraisalState(
      appraisal: state.appraisal!.copyWith(phone: action.phoneNumber),
      action: action,
    );
  }

  if (action is UpdateAudioPathAction) {
    return AppraisalState(
      appraisal: state.appraisal!.copyWith(audioPath: action.audioPath),
      action: action,
    );
  }

  if (action is UpdateVideoPathAction) {
    return AppraisalState(
      appraisal: state.appraisal!.copyWith(videoPath: action.videoPath),
      action: action,
    );
  }

  if (action is UpdateCommentAction) {
    return AppraisalState(
      appraisal: state.appraisal!.copyWith(comment: action.comment),
      action: action,
    );
  }

  if (action is UpdateAnswerAction) {
    return AppraisalState(
      appraisal:
          state.appraisal == null ? Appraisal(questionnaire: action.id) : state.appraisal!.copyWith(answer: action.id),
      action: action,
    );
  }

  if (action is SendRatingAction) {
    return AppraisalState(
      appraisal: null,
      action: action,
    );
  }

  if (action is AppraisalErrorAction) {
    return AppraisalState(
      appraisal: state.appraisal,
      action: action,
      errorMessage: action.errorMessage,
    );
  }

  return state;
}
