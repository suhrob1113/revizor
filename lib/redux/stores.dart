// Package imports:

// Package imports:
import 'package:get_it/get_it.dart';

// Project imports:
import 'package:revizor/redux/appraisal/store.dart';
import 'package:revizor/redux/authentication/store.dart';
import 'package:revizor/redux/systext/store.dart';
import 'package:revizor/redux/tablet/store.dart';

// Project imports:

class Stores {
  late AuthenticationStore _authenticationStore;
  late TabletStore _tabletStore;
  late AppraisalStore _appraisalStore;
  late SystextStore _systextStore;

  static void init() {
    final getIt = GetIt.instance;

    getIt.registerSingleton<Stores>(Stores());
    getIt<Stores>().create();
  }

  void create() {
    _authenticationStore = AuthenticationStore();
    _tabletStore = TabletStore();
    _appraisalStore = AppraisalStore();
    _systextStore = SystextStore();
  }

  AuthenticationStore get authentication => _authenticationStore;
  TabletStore get tablet => _tabletStore;
  AppraisalStore get appraisal => _appraisalStore;
  SystextStore get systext => _systextStore;
}
