// Project imports:
import 'package:revizor/api/models/authentication/authentication.dart';

class UpdateUsernameAction {
  final String username;

  UpdateUsernameAction(this.username);
}

class UpdatePasswordAction {
  final String password;

  UpdatePasswordAction(this.password);
}

class UpdateAuthenticationAction {
  final Authentication authentication;

  UpdateAuthenticationAction(this.authentication);
}

class AuthenticateAction {}

class AuthenticationErrorAction {
  final String errorMessage;

  AuthenticationErrorAction(this.errorMessage);
}
