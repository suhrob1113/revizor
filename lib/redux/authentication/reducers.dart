// Flutter imports:
import 'package:flutter/foundation.dart';

// Project imports:
import 'package:revizor/redux/authentication/actions.dart';
import 'store.dart';

AuthenticationState authenticationReducer(AuthenticationState state, action) {
  debugPrint('authenticationReducer: New Action $action');

  if (action is UpdateUsernameAction) {
    return AuthenticationState(
      username: action.username,
      password: state.password,
      authentication: state.authentication,
      action: action,
    );
  }

  if (action is UpdatePasswordAction) {
    return AuthenticationState(
      username: state.username,
      password: action.password,
      authentication: state.authentication,
      action: action,
    );
  }

  if (action is UpdateAuthenticationAction) {
    return AuthenticationState(
      username: state.username,
      password: state.password,
      authentication: action.authentication,
      action: action,
      isLoading: false,
    );
  }

  if (action is AuthenticationErrorAction) {
    return AuthenticationState(
      username: state.username,
      password: state.password,
      authentication: state.authentication,
      action: action,
      errorMessage: action.errorMessage,
      isLoading: false,
    );
  }

  if (action is AuthenticateAction) {
    return AuthenticationState(
      username: state.username,
      password: state.password,
      authentication: state.authentication,
      action: action,
      isLoading: true,
    );
  }

  return state;
}
