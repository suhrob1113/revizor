// Flutter imports:
import 'package:flutter/foundation.dart';

// Package imports:
import 'package:redux/redux.dart';

// Project imports:
import 'package:revizor/api/api_helper.dart';
import 'package:revizor/conf/routes/main_routes_constants.dart';
import 'package:revizor/conf/sp_keys/shared_preference_constants.dart';
import 'package:revizor/redux/authentication/actions.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/utils/shared_preference_helper.dart';
import 'store.dart';

void authenticationMiddleware(Store<AuthenticationState> store, action, NextDispatcher next) async {
  debugPrint('authenticationMiddleware: New Action ${action.toString()}');

  if (action is AuthenticateAction) {
    if (store.state.username == null ||
        store.state.username!.isEmpty ||
        store.state.password == null ||
        store.state.password!.isEmpty) {
      next(action);
      return;
    }
    next(action);
    final authenticationResonse =
        await RootService.api.authenticationApi.authenticate(store.state.username!, store.state.password!);
    if (authenticationResonse.isSuccessful) {
      store.dispatch(UpdateAuthenticationAction(authenticationResonse.body!));

      SPHelper.accessToken = authenticationResonse.body!.token;
      SPHelper.accessType = authenticationResonse.body!.type;

      RootService.api.create();

      RootService.tablet.fetch();

      RootService.navigator.popUNtilFirst();

      switch (authenticationResonse.body!.type) {
        case 1:
          RootService.navigator.pushReplacementNamed(MyRoutes.rateServicePage);
          break;
        case 2:
          RootService.navigator.pushReplacementNamed(MyRoutes.employeesPage);
          break;
        case 3:
          RootService.navigator.pushReplacementNamed(MyRoutes.departmentsPage);
          break;
        case 4:
          RootService.navigator.pushReplacementNamed(MyRoutes.rateWithBarner);
          break;
        case 5:
          RootService.navigator.pushReplacementNamed(MyRoutes.rateWithBarner);
          break;
        case 6:
          RootService.navigator.pushReplacementNamed(MyRoutes.rateWithChoice);
          break;
        case 7:
          RootService.navigator.pushReplacementNamed(MyRoutes.rateWithNps);
          break;
        default:
          break;
      }
      return;
    } else {
      if (SPHelper.accessToken == null) {
        store.dispatch(AuthenticationErrorAction(await getMsgFromError(authenticationResonse)));
        return;
      } else {
        SPHelper.remove(SPKeys.accessToken);
        SPHelper.remove(SPKeys.accessType);
        RootService.api.create();
        store.dispatch(AuthenticateAction());
      }
    }
  }
  next(action);
}
