// Project imports:
import 'package:revizor/api/models/authentication/authentication.dart';
import '../observable_store.dart';
import 'middleware.dart';
import 'reducers.dart';

class AuthenticationState extends ObservableState {
  final String? username;
  final String? password;
  final Authentication? authentication;

  final action;
  AuthenticationState({
    required this.username,
    required this.password,
    required this.authentication,
    required this.action,
    isLoading = false,
    errorMessage,
  }) : super(
          isLoading: isLoading,
          errorMessage: errorMessage,
        );
  factory AuthenticationState.init() => AuthenticationState(
        username: null,
        password: null,
        authentication: null,
        action: null,
      );
}

class AuthenticationStore extends ObservableStore<AuthenticationState, dynamic> {
  AuthenticationStore()
      : super(
          authenticationReducer,
          AuthenticationState.init(),
          [authenticationMiddleware],
        );
}
