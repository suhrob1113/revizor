// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Package imports:
import 'package:intl/date_symbol_data_local.dart';

// Project imports:
import 'app.dart';
import 'services/root_service.dart';
import 'utils/shared_preference_helper.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  if (!kIsWeb)
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);

  // Логгер для http запросов
  // LogHelper.init();

  await SPHelper.init();

  RootService.init();

  initializeDateFormatting();

  runApp(
    MyApp(),
  );
}
