// Package imports:
import 'package:shared_preferences/shared_preferences.dart';

// Project imports:
import 'package:revizor/conf/sp_keys/shared_preference_constants.dart';

// Project imports:

class SPHelper {
  static SharedPreferences? prefs;

  static Future<void> init() async {
    if (prefs == null) {
      prefs = await SharedPreferences.getInstance();
    }
  }

  static int get themeId => prefs?.getInt(SPKeys.themeId) ?? 1;
  static set themeId(int value) => prefs?.setInt(SPKeys.themeId, value);

  static String? get accessToken => prefs?.getString(SPKeys.accessToken) ?? null;
  static set accessToken(String? value) => prefs?.setString(SPKeys.accessToken, value!);

  static String get language => prefs?.getString(SPKeys.language) ?? 'ru';
  static set language(String value) => prefs?.setString(SPKeys.language, value);

  static int? get accessType => prefs?.getInt(SPKeys.accessType) ?? null;
  static set accessType(int? value) => prefs?.setInt(SPKeys.accessType, value!);

  static List<String> get unsuccessfulAppraisalList => prefs?.getStringList(SPKeys.unsuccessfulAppraisalList) ?? [];
  static set unsuccessfulAppraisalList(List<String> value) =>
      prefs?.setStringList(SPKeys.unsuccessfulAppraisalList, value);

  static void remove(String key) {
    prefs?.remove(key);
  }

  static Future<bool>? clear() => prefs?.clear();

  static Future logout() async {
    clear();
  }
}
