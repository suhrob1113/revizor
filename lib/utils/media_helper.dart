// Flutter imports:
import 'package:flutter/material.dart';

class MediaHelper {
  static var width;
  static var height;

  static init(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    width = size.width;
    height = size.height;
  }
}
