// Package imports:
import 'package:url_launcher/url_launcher.dart';

void launchURL(String urlFrom) async {
  final url = urlFrom;

  try {
    launch(url);
  } catch (e) {
    print('Could not launch $url');
  }
}
