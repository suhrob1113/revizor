class MyPhoneFormatter {
  //
  //
  static String formatPhoneRu(phone) {
    if (phone.length != 11) return phone;
    var p = phone;
    var formattedPhone =
        '+${p[0]} (${p[1]}${p[2]}${p[3]}) ${p[4]}${p[5]}${p[6]} ${p[7]}${p[8]} ${p[9]}${p[10]}';

    return formattedPhone;
  }
}
