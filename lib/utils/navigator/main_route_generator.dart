// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/conf/routes/main_routes_constants.dart';
import 'package:revizor/ui/authentication/authentication_page.dart';
import 'package:revizor/ui/camera/camera_page.dart';
import 'package:revizor/ui/camera/web_camera_page.dart';
import 'package:revizor/ui/department/departments_page.dart';
import 'package:revizor/ui/employee/employes_page.dart';
import 'package:revizor/ui/error/error_page.dart';
import 'package:revizor/ui/rating/rate_service_page.dart';
import 'package:revizor/ui/rating/rate_with_banner_page.dart';
import 'package:revizor/ui/rating/rate_with_choice_page.dart';
import 'package:revizor/ui/rating/rate_with_nps_page.dart';

// Project imports:

class MainRouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // final args = settings.arguments;

    switch (settings.name) {
      case MyRoutes.authenticationPage:
        return MaterialPageRoute(
          settings: RouteSettings(name: settings.name),
          builder: (_) => AuthenticationPage(),
        );
      case MyRoutes.departmentsPage:
        return MaterialPageRoute(
          settings: RouteSettings(name: settings.name),
          builder: (_) => DepartmentsPage(),
        );
      case MyRoutes.employeesPage:
        return MaterialPageRoute(
          settings: RouteSettings(name: settings.name),
          builder: (_) => EmployesPage(),
        );
      case MyRoutes.rateServicePage:
        return MaterialPageRoute(
          settings: RouteSettings(name: settings.name),
          builder: (_) => RateServicePage(),
        );
      case MyRoutes.rateWithBarner:
        return MaterialPageRoute(
          settings: RouteSettings(name: settings.name),
          builder: (_) => RateWithBannerPage(),
        );
      case MyRoutes.rateWithChoice:
        return MaterialPageRoute(
          settings: RouteSettings(name: settings.name),
          builder: (_) => RateWithChoicePage(),
        );
      case MyRoutes.rateWithNps:
        return MaterialPageRoute(
          settings: RouteSettings(name: settings.name),
          builder: (_) => RateWithNpsPage(),
        );
      case MyRoutes.cameraPage:
        return MaterialPageRoute(
          settings: RouteSettings(name: settings.name),
          builder: (_) => CameraPage(),
        );
      case MyRoutes.webCameraPage:
        return MaterialPageRoute(
          settings: RouteSettings(name: settings.name),
          builder: (_) => WebcameraPage(),
        );

      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) => ErrorPage());
  }
}
