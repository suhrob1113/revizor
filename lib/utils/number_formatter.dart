// Package imports:
import 'package:intl/intl.dart';

class MyFormatter {
  static toNum(double number) {
    final formatter = NumberFormat("#,###.##");
    return formatter.format(number);
  }

  static toNumFixed(double number) {
    final formatter = NumberFormat("####");
    return formatter.format(number);
  }

  static toDecNum(double number) {
    final formatter = NumberFormat("#,###.#####");
    return formatter.format(number);
  }

  static normalize(double d) {
    return (d > 1000000 || d < -1000000)
        ? toCompact(d)
        : (d > 1 || d < -1)
            ? toNum(d)
            : toDecNum(d);
  }

  static toCompact(number) {
    final formatter =
        NumberFormat.compactCurrency(decimalDigits: 2, symbol: '');
    return formatter.format(number);
  }
}
