// Package imports:
import 'package:chopper/chopper.dart';

// Project imports:
import 'package:revizor/api/models/tablet/tablet.dart';
import 'package:revizor/conf/api/url_constants.dart';

part 'tablet_api.chopper.dart';

@ChopperApi(baseUrl: MyUrl.baseUrl)
abstract class TabletApi extends ChopperService {
  static TabletApi create([ChopperClient? client]) => _$TabletApi(client);

  @Get(path: MyUrl.data)
  Future<Response<Tablet>> fetch();
}
