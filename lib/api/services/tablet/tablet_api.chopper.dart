// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tablet_api.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$TabletApi extends TabletApi {
  _$TabletApi([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = TabletApi;

  @override
  Future<Response<Tablet>> fetch() {
    final $url = 'https://lagoms.uz/api/thetablet/data/';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<Tablet, Tablet>($request);
  }
}
