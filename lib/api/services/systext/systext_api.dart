// Package imports:
import 'package:chopper/chopper.dart';

// Project imports:
import 'package:revizor/api/models/systext/systext.dart';
import 'package:revizor/conf/api/url_constants.dart';

part 'systext_api.chopper.dart';

@ChopperApi(baseUrl: MyUrl.baseUrl)
abstract class SystextApi extends ChopperService {
  static SystextApi create([ChopperClient? client]) => _$SystextApi(client);

  @Get(path: MyUrl.systextList)
  Future<Response<List<Systext>>> getTexts();
}
