// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'systext_api.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$SystextApi extends SystextApi {
  _$SystextApi([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = SystextApi;

  @override
  Future<Response<List<Systext>>> getTexts() {
    final $url = 'https://lagoms.uz/api/thetablet/systext/list/';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<Systext>, Systext>($request);
  }
}
