// Package imports:
import 'package:chopper/chopper.dart';

// Project imports:
import 'package:revizor/conf/api/url_constants.dart';

part 'appraisal_api.chopper.dart';

@ChopperApi(baseUrl: MyUrl.baseUrl)
abstract class AppraisalApi extends ChopperService {
  static AppraisalApi create([ChopperClient? client]) => _$AppraisalApi(client);

  // @Post(path: MyUrl.appraisal, headers: {contentTypeKey: formEncodedHeaders})
  // @FactoryConverter(request: FormUrlEncodedConverter.requestFactory)
  // @Multipart()
  // Future<Response> rateAsForm({
  //   @Body() Map<String, dynamic>? body,
  //   @PartFile('audio') String? audioPath,
  //   @PartFile('video') String? videoPath,
  // });

  // @Post(path: MyUrl.appraisal)
  // @Multipart()
  // Future<Response> rateAsQuery({
  //   @QueryMap() Map<String, dynamic>? queryMap,
  //   @PartFile('audio') String? audioPath,
  //   @PartFile('video') String? videoPath,
  // });

  // @Post(
  //   path: MyUrl.appraisal,
  //   headers: {contentTypeKey: formEncodedHeaders},
  // )
  // @Multipart()
  // @FactoryConverter(request: FormUrlEncodedConverter.requestFactory)
  // Future<Response> rateWithFields({
  //   @Field() int? star,
  //   @Field() int? employee,
  //   @Field() int? department,
  //   @Field() int? service,
  //   @Field() int? answer,
  //   @Field() String? phone,
  //   @Field() String? comment,
  //   @PartFile('audio') String? audioPath,
  //   @PartFile('video') String? videoPath,
  // });

  // Future<Response> rate({
  //   @Query() int? star,
  //   @Query() int? employee,
  //   @Query() int? department,
  //   @Query() int? service,
  //   @Query() int? answer,
  //   @Query() String? phone,
  //   @Query() String? comment,
  //   @PartFile('audio') String? audioPath,
  //   @PartFile('video') String? videoPath,
  // });

  // @Post(path: MyUrl.appraisal)
  // @Multipart()
  // Future<Response> rate({
  //   @Field() int? star,
  //   @Field() int? employee,
  //   @Field() int? department,
  //   @Field() int? service,
  //   @Field() int? answer,
  //   @Field() String? phone,
  //   @Field() String? comment,
  //   @PartFile('audio') String? audioPath,
  //   @PartFile('video') String? videoPath,
  // });
}
