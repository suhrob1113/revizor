// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appraisal_api.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$AppraisalApi extends AppraisalApi {
  _$AppraisalApi([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = AppraisalApi;
}
