// Dart imports:
import 'dart:typed_data';

// Package imports:
import 'package:http/http.dart' as http;

class BlobHelper {
  static Future<Uint8List> getBlobBytes(String url) async {
    http.Response response = await http.get(Uri.parse(url));
    print('lengthInBytes');
    print(response.bodyBytes.lengthInBytes);
    return response.bodyBytes; //Uint8List
  }
}
