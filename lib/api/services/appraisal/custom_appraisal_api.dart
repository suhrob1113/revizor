import 'package:dio/dio.dart';

// Project imports:
import 'package:revizor/api/models/appraisal/appraisal.dart';
import 'package:revizor/conf/api/url_constants.dart';
import 'package:revizor/utils/shared_preference_helper.dart';
import 'blob_helper.dart';

class CustomAppraisalApi {
  static Future<bool> customAppraisalRequest(
    Appraisal appraisal, {
    bool isWeb = false,
  }) async {
    final dio = Dio(BaseOptions(baseUrl: MyUrl.baseUrl, headers: {'Authorization': 'Token ${SPHelper.accessToken}'}));
    var formData = FormData.fromMap({
      if (appraisal.star != null) 'star': appraisal.star,
      if (appraisal.employee != null) 'employee': appraisal.employee,
      if (appraisal.department != null) 'department': appraisal.department,
      if (appraisal.service != null) 'service': appraisal.service,
      if (appraisal.phone != null) 'phone': appraisal.phone,
      if (appraisal.comment != null) 'comment': appraisal.comment,
      if (appraisal.questionnaire != null) 'questionnaire': appraisal.questionnaire,
      if (appraisal.nps != null) 'nps': appraisal.nps,
      if (appraisal.audioPath != null && !isWeb) 'audio': await MultipartFile.fromFile(appraisal.audioPath!),
      if (appraisal.videoPath != null && !isWeb) 'video': await MultipartFile.fromFile(appraisal.videoPath!),
      if (appraisal.videoPath != null && isWeb)
        'video': MultipartFile.fromBytes(await BlobHelper.getBlobBytes(appraisal.videoPath!),
            filename: DateTime.now().millisecondsSinceEpoch.toString() + '.mp4'),
      if (appraisal.audioPath != null && isWeb)
        'audio': MultipartFile.fromBytes(await BlobHelper.getBlobBytes(appraisal.audioPath!),
            filename: DateTime.now().millisecondsSinceEpoch.toString() + '.mp3'),
    });

    try {
      final response = await dio.post(MyUrl.appraisal, data: formData);

      if (response.statusCode! >= 200 && response.statusCode! < 300) {
        return true;
      } else
        return false;
    } catch (e) {
      return false;
    }
  }
}
