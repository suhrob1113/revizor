// // GENERATED CODE - DO NOT MODIFY BY HAND

// part of 'appraisal_api.dart';

// // **************************************************************************
// // ChopperGenerator
// // **************************************************************************

// // ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
// class _$AppraisalApi extends AppraisalApi {
//   _$AppraisalApi([ChopperClient? client]) {
//     if (client == null) return;
//     this.client = client;
//   }

//   @override
//   final definitionType = AppraisalApi;

//   @override
//   Future<Response<dynamic>> rate(
//       {int? star,
//       int? employee,
//       int? department,
//       int? service,
//       int? answer,
//       String? phone,
//       String? comment,
//       String? audioPath,
//       String? videoPath}) {
//     final $url = 'http://185.196.214.60/api/thetablet/appraisal/';
//     final $params = <String, dynamic>{
//       if (star != null) 'star': star,
//       if (employee != null) 'employee': employee,
//       if (department != null) 'department': department,
//       if (service != null) 'service': service,
//       if (answer != null) 'answer': answer,
//       if (phone != null) 'phone': phone,
//       if (comment != null) 'comment': comment
//     };
//     final $parts = <PartValue>[
//       if (audioPath != null) PartValueFile<String>('audio', audioPath),
//       if (videoPath != null) PartValueFile<String>('video', videoPath)
//     ];
//     final $request =
//         Request('POST', $url, client.baseUrl, parts: $parts, multipart: $parts.isNotEmpty, parameters: $params);
//     return client.send<dynamic, dynamic>($request);
//   }
// }
