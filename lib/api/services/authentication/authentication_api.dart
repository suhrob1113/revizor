// Package imports:
import 'package:chopper/chopper.dart';

// Project imports:
import 'package:revizor/api/models/authentication/authentication.dart';
import 'package:revizor/conf/api/url_constants.dart';

part 'authentication_api.chopper.dart';

@ChopperApi(baseUrl: MyUrl.baseUrl)
abstract class AuthenticationApi extends ChopperService {
  static AuthenticationApi create([ChopperClient? client]) => _$AuthenticationApi(client);

  @Post(path: MyUrl.account)
  Future<Response<Authentication>> authenticate(
    @Field() String username,
    @Field() String password,
  );
}
