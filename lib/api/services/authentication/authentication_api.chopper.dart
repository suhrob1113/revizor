// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authentication_api.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$AuthenticationApi extends AuthenticationApi {
  _$AuthenticationApi([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = AuthenticationApi;

  @override
  Future<Response<Authentication>> authenticate(
      String username, String password) {
    final $url = 'https://lagoms.uz/api/thetablet/account/';
    final $body = <String, dynamic>{'username': username, 'password': password};
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<Authentication, Authentication>($request);
  }
}
