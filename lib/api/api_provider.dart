// Dart imports:
import 'dart:io';

// Package imports:
import 'package:chopper/chopper.dart';
import 'package:get_it/get_it.dart';

// Project imports:
import 'package:revizor/api/services/appraisal/appraisal_api.dart';
import 'package:revizor/api/services/authentication/authentication_api.dart';
import 'package:revizor/api/services/systext/systext_api.dart';
import 'package:revizor/api/services/tablet/tablet_api.dart';
import 'package:revizor/utils/shared_preference_helper.dart';
import 'custom_convertor.dart';

// Project imports:

class ApiProvider {
  late ChopperClient _client;

  late AuthenticationApi authenticationApi;
  late AppraisalApi appraisalApi;
  late TabletApi tabletApi;
  late SystextApi systextApi;

  static void init() {
    final getIt = GetIt.instance;

    getIt.registerSingleton<ApiProvider>(ApiProvider());
    getIt<ApiProvider>().create();
  }

  void create() {
    _client = ChopperClient(
      // baseUrl: MyUrl.apiUrlV2,
      services: [
        AuthenticationApi.create(),
        TabletApi.create(),
        AppraisalApi.create(),
        SystextApi.create(),
      ],
      interceptors: _interceptors(),
      converter: MyConverter(),
      errorConverter: MyConverter(),
    );

    authenticationApi = _client.getService<AuthenticationApi>();
    tabletApi = _client.getService<TabletApi>();
    appraisalApi = _client.getService<AppraisalApi>();
    systextApi = _client.getService<SystextApi>();
  }

  static List _interceptors() {
    List interceptors = [];
    interceptors..add(HttpLoggingInterceptor());

    final token = SPHelper.accessToken;
    final language = SPHelper.language;

    if (token != null) {
      interceptors
        ..add(
          HeadersInterceptor({HttpHeaders.authorizationHeader: 'Token $token'}),
        );
    }
    interceptors
      ..add(
        HeadersInterceptor({'Accept-Language': language}),
      );

    return interceptors;
  }

  void dispose() {
    authenticationApi.client.dispose();
    appraisalApi.client.dispose();
    tabletApi.client.dispose();
    systextApi.client.dispose();
  }
}
