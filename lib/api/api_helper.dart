// Package imports:

// Package imports:
import 'package:chopper/chopper.dart';
import 'package:http/http.dart' as http;

// Project imports:
import 'package:revizor/utils/connectivity_helper.dart';

// Project imports:

// Project imports:

Future request(Future req, {int seconds = 30}) async {
  if (await ConnectivityHelper.isConnected()) {
    return req.timeout(Duration(seconds: seconds), onTimeout: () {
      return Response(
        http.Response('', 408),
        null,
        error: 'The request has timed out',
      );
    });
  } else {
    return Response(http.Response('', 503), null, error: 'No connection');
  }
}

Future<String> getMsgFromError(Response response) async {
  String message;
  if (response.statusCode == 408)
    message = 'The request has timed out';
  else if (response.statusCode == 503)
    message = 'It seems there are some problems with your internet connection. Please, try again later.';
  else if (response.error != null && response.error is Map) {
    message = (response.error as Map)['detail'] ?? (response.error as Map)['error'];
  } else
    message = 'Server error ${response.statusCode}';
  return message;
}
