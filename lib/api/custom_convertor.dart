// Package imports:
import 'package:chopper/chopper.dart';

// Project imports:
import 'package:revizor/api/models/account/account.dart';
import 'package:revizor/api/models/answer/answer.dart';
import 'package:revizor/api/models/authentication/authentication.dart';
import 'package:revizor/api/models/banner/banner.dart';
import 'package:revizor/api/models/department/department.dart';
import 'package:revizor/api/models/emoji/emoji.dart';
import 'package:revizor/api/models/employee/employee.dart';
import 'package:revizor/api/models/question/question.dart';
import 'package:revizor/api/models/service_info/service_info.dart';
import 'package:revizor/api/models/tablet/tablet.dart';
import 'models/nps/nps.dart';
import 'models/systext/systext.dart';

// Project imports:

// Project imports:

class MyConverter extends JsonConverter {
  @override
  Response<BodyType> convertResponse<BodyType, SingleItemType>(Response response) {
    if ('${response.body}'.isEmpty) return response.body;

    final Response dynamicResponse = super.convertResponse(response);
    var body = dynamicResponse.body;

    final BodyType customBody = _convertToCustomObject<BodyType, SingleItemType>(body);
    return dynamicResponse.copyWith<BodyType>(body: customBody);
  }

  BodyType _convertToCustomObject<BodyType, SingleItemType>(dynamic element) {
    if (element is List)
      return _deserializeListOf<BodyType, SingleItemType>(element);
    else
      return _deserialize<SingleItemType>(element);
  }

  dynamic _deserializeListOf<BodyType, SingleItemType>(List dynamicList) {
    List<SingleItemType> list =
        dynamicList.map<SingleItemType>((element) => _deserialize<SingleItemType>(element)).toList();
    return list;
  }

  _deserialize<SingleItemType>(Map<String, dynamic> json) {
    switch (SingleItemType) {
      case Account:
        return Account.fromJson(json);
      case Answer:
        return Answer.fromJson(json);
      case Authentication:
        return Authentication.fromJson(json);
      case Banner:
        return Banner.fromJson(json);
      case Department:
        return Department.fromJson(json);
      case Systext:
        return Systext.fromJson(json);
      case Emoji:
        return Emoji.fromJson(json);
      case EmojiList:
        return EmojiList.fromJson(json);
      case Employee:
        return Employee.fromJson(json);
      case Question:
        return Question.fromJson(json);
      case ServiceInfo:
        return ServiceInfo.fromJson(json);
      case Tablet:
        return Tablet.fromJson(json);
      case Nps:
        return Nps.fromJson(json);
      case NpsEmojiList:
        return NpsEmojiList.fromJson(json);
      default:
        return null;
    }
  }
}
