// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appraisal.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension AppraisalCopyWith on Appraisal {
  Appraisal copyWith({
    int? answer,
    String? audioPath,
    String? comment,
    int? department,
    int? employee,
    int? nps,
    String? phone,
    int? service,
    int? star,
    String? videoPath,
  }) {
    return Appraisal(
      questionnaire: answer ?? this.questionnaire,
      audioPath: audioPath ?? this.audioPath,
      comment: comment ?? this.comment,
      department: department ?? this.department,
      employee: employee ?? this.employee,
      nps: nps ?? this.nps,
      phone: phone ?? this.phone,
      service: service ?? this.service,
      star: star ?? this.star,
      videoPath: videoPath ?? this.videoPath,
    );
  }

  Appraisal copyWithNull({
    bool answer = false,
    bool audioPath = false,
    bool comment = false,
    bool department = false,
    bool employee = false,
    bool nps = false,
    bool phone = false,
    bool service = false,
    bool star = false,
    bool videoPath = false,
  }) {
    return Appraisal(
      questionnaire: answer == true ? null : this.questionnaire,
      audioPath: audioPath == true ? null : this.audioPath,
      comment: comment == true ? null : this.comment,
      department: department == true ? null : this.department,
      employee: employee == true ? null : this.employee,
      nps: nps == true ? null : this.nps,
      phone: phone == true ? null : this.phone,
      service: service == true ? null : this.service,
      star: star == true ? null : this.star,
      videoPath: videoPath == true ? null : this.videoPath,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Appraisal _$AppraisalFromJson(Map<String, dynamic> json) {
  return Appraisal(
    star: json['star'] as int?,
    employee: json['employee'] as int?,
    service: json['service'] as int?,
    department: json['department'] as int?,
    questionnaire: json['answer'] as int?,
    nps: json['nps'] as int?,
    phone: json['phone'] as String?,
    comment: json['comment'] as String?,
    audioPath: json['audioPath'] as String?,
    videoPath: json['videoPath'] as String?,
  );
}

Map<String, dynamic> _$AppraisalToJson(Appraisal instance) => <String, dynamic>{
      'star': instance.star,
      'employee': instance.employee,
      'service': instance.service,
      'department': instance.department,
      'answer': instance.questionnaire,
      'nps': instance.nps,
      'phone': instance.phone,
      'comment': instance.comment,
      'audioPath': instance.audioPath,
      'videoPath': instance.videoPath,
    };
