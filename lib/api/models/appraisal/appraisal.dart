import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';

part 'appraisal.g.dart';

@CopyWith(generateCopyWithNull: true)
@JsonSerializable()
class Appraisal {
  @JsonKey()
  final int? star;
  @JsonKey()
  final int? employee;
  @JsonKey()
  final int? service;
  @JsonKey()
  final int? department;
  @JsonKey()
  final int? questionnaire;
  @JsonKey()
  final int? nps;
  @JsonKey()
  final String? phone;
  @JsonKey()
  final String? comment;
  @JsonKey()
  final String? audioPath;
  @JsonKey()
  final String? videoPath;

  Appraisal({
    this.star,
    this.employee,
    this.service,
    this.department,
    this.questionnaire,
    this.nps,
    this.phone,
    this.comment,
    this.audioPath,
    this.videoPath,
  });

  factory Appraisal.fromJson(Map<String, dynamic> json) => _$AppraisalFromJson(json);

  Map<String, dynamic> toJson() => _$AppraisalToJson(this);
}
