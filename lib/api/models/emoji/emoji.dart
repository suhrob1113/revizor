// Package imports:
import 'package:json_annotation/json_annotation.dart';

part 'emoji.g.dart';

@JsonSerializable(createToJson: false)
class Emoji {
  @JsonKey(defaultValue: 0)
  final int? appraisal;

  @JsonKey(defaultValue: '')
  final String? emoji;

  @JsonKey(defaultValue: '')
  final String? text;

  Emoji({
    this.appraisal,
    this.emoji,
    this.text,
  });

  factory Emoji.fromJson(Map<String, dynamic> json) => _$EmojiFromJson(json);
}

@JsonSerializable(createToJson: false)
class EmojiList {
  @JsonKey(name: 'kit_id')
  final int? kitId;

  @JsonKey()
  final List<Emoji>? items;

  EmojiList({
    this.kitId,
    this.items,
  });

  factory EmojiList.fromJson(Map<String, dynamic> json) => _$EmojiListFromJson(json);
}
