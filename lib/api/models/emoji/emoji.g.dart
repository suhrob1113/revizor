// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'emoji.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Emoji _$EmojiFromJson(Map<String, dynamic> json) {
  return Emoji(
    appraisal: json['appraisal'] as int? ?? 0,
    emoji: json['emoji'] as String? ?? '',
    text: json['text'] as String? ?? '',
  );
}

EmojiList _$EmojiListFromJson(Map<String, dynamic> json) {
  return EmojiList(
    kitId: json['kit_id'] as int?,
    items: (json['items'] as List<dynamic>?)
        ?.map((e) => Emoji.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}
