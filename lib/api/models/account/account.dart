// Package imports:
import 'package:json_annotation/json_annotation.dart';

part 'account.g.dart';

@JsonSerializable(createToJson: false)
class Account {
  @JsonKey()
  final int? id;

  @JsonKey(defaultValue: '')
  final String? username;

  Account({
    this.id,
    this.username,
  });

  factory Account.fromJson(Map<String, dynamic> json) => _$AccountFromJson(json);
}
