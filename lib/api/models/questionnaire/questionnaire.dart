// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:revizor/api/models/answer/answer.dart';

part 'questionnaire.g.dart';

@JsonSerializable(createToJson: false)
class Questionnaire {
  @JsonKey()
  final int? id;

  @JsonKey(name: 'message_type')
  final int? messageType;

  @JsonKey()
  final String? text;

  @JsonKey()
  final int? parent;

  Questionnaire({
    this.id,
    this.messageType,
    this.parent,
    this.text,
  });

  factory Questionnaire.fromJson(Map<String, dynamic> json) => _$QuestionnaireFromJson(json);
}
