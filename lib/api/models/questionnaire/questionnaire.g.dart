// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'questionnaire.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Questionnaire _$QuestionnaireFromJson(Map<String, dynamic> json) {
  return Questionnaire(
    id: json['id'] as int?,
    messageType: json['message_type'] as int?,
    parent: json['parent'] as int?,
    text: json['text'] as String?,
  );
}
