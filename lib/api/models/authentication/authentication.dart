// Package imports:
import 'package:json_annotation/json_annotation.dart';

part 'authentication.g.dart';

@JsonSerializable(createToJson: false)
class Authentication {
  @JsonKey()
  final String? token;

  @JsonKey(name: 'the_tablet_type')
  final int? type;

  Authentication({
    this.token,
    this.type,
  });

  factory Authentication.fromJson(Map<String, dynamic> json) => _$AuthenticationFromJson(json);
}
