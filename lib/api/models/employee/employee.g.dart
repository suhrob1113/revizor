// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'employee.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Employee _$EmployeeFromJson(Map<String, dynamic> json) {
  return Employee(
    id: json['id'] as int?,
    name: json['name'] as String? ?? '',
    text: json['text'] as String? ?? '',
    position: json['position'] as String? ?? '',
    photo: json['photo'] as String? ?? '',
    rating: (json['rating'] as num?)?.toDouble() ?? 0,
    votes: json['votes'] as int? ?? 0,
  );
}
