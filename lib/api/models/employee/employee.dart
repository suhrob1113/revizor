// Package imports:
import 'package:json_annotation/json_annotation.dart';

part 'employee.g.dart';

@JsonSerializable(createToJson: false)
class Employee {
  @JsonKey()
  final int? id;

  @JsonKey(defaultValue: '')
  final String? name;

  @JsonKey(defaultValue: '')
  final String? text;

  @JsonKey(defaultValue: '')
  final String? position;

  @JsonKey(defaultValue: '')
  final String? photo;

  @JsonKey(defaultValue: 0)
  final double? rating;

  @JsonKey(defaultValue: 0)
  final int? votes;

  Employee({
    this.id,
    this.name,
    this.text,
    this.position,
    this.photo,
    this.rating,
    this.votes,
  });

  factory Employee.fromJson(Map<String, dynamic> json) => _$EmployeeFromJson(json);
}
