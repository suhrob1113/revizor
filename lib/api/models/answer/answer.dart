// Package imports:
import 'package:json_annotation/json_annotation.dart';
import 'package:revizor/api/models/question/question.dart';

part 'answer.g.dart';

@JsonSerializable(createToJson: false)
class Answer {
  @JsonKey()
  final int? id;

  @JsonKey(defaultValue: '')
  final String? text;

  @JsonKey(name: 'tree_id')
  final int? treeId;

  @JsonKey()
  final int? level;

  @JsonKey()
  final int? parent;

  @JsonKey()
  final Question? question;

  Answer({
    this.id,
    this.text,
    this.treeId,
    this.level,
    this.parent,
    this.question,
  });

  factory Answer.fromJson(Map<String, dynamic> json) => _$AnswerFromJson(json);
}
