// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'answer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Answer _$AnswerFromJson(Map<String, dynamic> json) {
  return Answer(
    id: json['id'] as int?,
    text: json['text'] as String? ?? '',
    treeId: json['tree_id'] as int?,
    level: json['level'] as int?,
    parent: json['parent'] as int?,
    question: json['question'] == null
        ? null
        : Question.fromJson(json['question'] as Map<String, dynamic>),
  );
}
