// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'department.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Department _$DepartmentFromJson(Map<String, dynamic> json) {
  return Department(
    id: json['id'] as int?,
    employees: (json['employees'] as List<dynamic>?)
        ?.map((e) => Employee.fromJson(e as Map<String, dynamic>))
        .toList(),
    name: json['name'] as String? ?? '',
    bannerImage: json['banner_image'] as String? ?? '',
    bannerTitle: json['banner_title'] as String? ?? '',
    text: json['text'] as String? ?? '',
  );
}
