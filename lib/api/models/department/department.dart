// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:revizor/api/models/employee/employee.dart';

part 'department.g.dart';

@JsonSerializable(createToJson: false)
class Department {
  @JsonKey()
  final int? id;

  @JsonKey()
  final List<Employee>? employees;

  @JsonKey(defaultValue: '')
  final String? name;

  @JsonKey(defaultValue: '', name: 'banner_title')
  final String? bannerTitle;

  @JsonKey(defaultValue: '', name: 'banner_image')
  final String? bannerImage;

  @JsonKey(defaultValue: '')
  final String? text;

  Department({
    this.id,
    this.employees,
    this.name,
    this.bannerImage,
    this.bannerTitle,
    this.text,
  });

  factory Department.fromJson(Map<String, dynamic> json) => _$DepartmentFromJson(json);
}
