// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:revizor/api/models/answer/answer.dart';

part 'question.g.dart';

@JsonSerializable(createToJson: false)
class Question {
  @JsonKey()
  final int? id;

  @JsonKey(defaultValue: '')
  final String? text;

  @JsonKey()
  final List<Answer>? answers;

  Question({
    this.id,
    this.text,
    this.answers,
  });

  factory Question.fromJson(Map<String, dynamic> json) => _$QuestionFromJson(json);
}
