// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:revizor/api/models/account/account.dart';
import 'package:revizor/api/models/department/department.dart';
import 'package:revizor/api/models/emoji/emoji.dart';
import 'package:revizor/api/models/employee/employee.dart';
import 'package:revizor/api/models/nps/nps.dart';
import 'package:revizor/api/models/question/question.dart';
import 'package:revizor/api/models/questionnaire/questionnaire.dart';
import 'package:revizor/api/models/service_info/service_info.dart';

part 'tablet.g.dart';

@JsonSerializable(createToJson: false)
class Tablet {
  @JsonKey()
  final int? id;

  @JsonKey(name: 'service_info')
  final ServiceInfo? serviceInfo;

  @JsonKey()
  final Employee? employee;

  @JsonKey()
  final Department? department;

  @JsonKey()
  final List<Department>? departments;

  @JsonKey()
  final List<Questionnaire>? questionnaire;

  @JsonKey()
  final Account? account;

  @JsonKey()
  final EmojiList? emojis;

  @JsonKey()
  final Nps? nps;

  @JsonKey(name: 'the_tablet_type')
  final int? type;

  @JsonKey()
  final int? theme;

  @JsonKey()
  final bool? status;

  Tablet({
    this.id,
    this.serviceInfo,
    this.employee,
    this.department,
    this.departments,
    this.questionnaire,
    this.account,
    this.emojis,
    this.nps,
    this.type,
    this.theme,
    this.status,
  });

  factory Tablet.fromJson(Map<String, dynamic> json) => _$TabletFromJson(json);
}
