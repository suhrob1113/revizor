// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tablet.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Tablet _$TabletFromJson(Map<String, dynamic> json) {
  return Tablet(
    id: json['id'] as int?,
    serviceInfo: json['service_info'] == null
        ? null
        : ServiceInfo.fromJson(json['service_info'] as Map<String, dynamic>),
    employee: json['employee'] == null
        ? null
        : Employee.fromJson(json['employee'] as Map<String, dynamic>),
    department: json['department'] == null
        ? null
        : Department.fromJson(json['department'] as Map<String, dynamic>),
    departments: (json['departments'] as List<dynamic>?)
        ?.map((e) => Department.fromJson(e as Map<String, dynamic>))
        .toList(),
    questionnaire: (json['questionnaire'] as List<dynamic>?)
        ?.map((e) => Questionnaire.fromJson(e as Map<String, dynamic>))
        .toList(),
    account: json['account'] == null
        ? null
        : Account.fromJson(json['account'] as Map<String, dynamic>),
    emojis: json['emojis'] == null
        ? null
        : EmojiList.fromJson(json['emojis'] as Map<String, dynamic>),
    nps: json['nps'] == null
        ? null
        : Nps.fromJson(json['nps'] as Map<String, dynamic>),
    type: json['the_tablet_type'] as int?,
    theme: json['theme'] as int?,
    status: json['status'] as bool?,
  );
}
