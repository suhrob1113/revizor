// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServiceInfo _$ServiceInfoFromJson(Map<String, dynamic> json) {
  return ServiceInfo(
    id: json['id'] as int?,
    name: json['name'] as String? ?? '',
    bannerImage: json['banner_image'] as String?,
    text: json['text'] as String? ?? '',
    title: json['title'] as String? ?? '',
    logo: json['logo'] as String? ?? '',
  );
}
