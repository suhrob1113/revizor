// Package imports:
import 'package:json_annotation/json_annotation.dart';

part 'service_info.g.dart';

@JsonSerializable(createToJson: false)
class ServiceInfo {
  @JsonKey()
  final int? id;

  @JsonKey(defaultValue: '')
  final String? name;

  @JsonKey(name: 'banner_image')
  final String? bannerImage;

  @JsonKey(defaultValue: '')
  final String? text;

  @JsonKey(defaultValue: '')
  final String? title;

  @JsonKey(defaultValue: '')
  final String? logo;

  ServiceInfo({
    this.id,
    this.name,
    this.bannerImage,
    this.text,
    this.title,
    this.logo,
  });

  factory ServiceInfo.fromJson(Map<String, dynamic> json) => _$ServiceInfoFromJson(json);
}
