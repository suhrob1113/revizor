// Package imports:
import 'package:json_annotation/json_annotation.dart';

part 'systext.g.dart';

@JsonSerializable(createToJson: false)
class Systext {
  @JsonKey()
  final int? id;

  @JsonKey(defaultValue: '')
  final String? key;

  @JsonKey(defaultValue: '')
  final String? value;

  Systext({
    this.id,
    this.key,
    this.value,
  });

  factory Systext.fromJson(Map<String, dynamic> json) => _$SystextFromJson(json);
}
