// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'systext.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Systext _$SystextFromJson(Map<String, dynamic> json) {
  return Systext(
    id: json['id'] as int?,
    key: json['key'] as String? ?? '',
    value: json['value'] as String? ?? '',
  );
}
