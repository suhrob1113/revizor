// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:revizor/api/models/emoji/emoji.dart';

part 'nps.g.dart';

@JsonSerializable(createToJson: false)
class Nps {
  @JsonKey()
  final int? id;

  @JsonKey(name: 'nps_emojis')
  final NpsEmojiList? npsEmojis;

  @JsonKey(defaultValue: '')
  final String? text;

  Nps({
    this.id,
    this.text,
    this.npsEmojis,
  });

  factory Nps.fromJson(Map<String, dynamic> json) => _$NpsFromJson(json);
}

@JsonSerializable(createToJson: false)
class NpsEmojiList {
  @JsonKey()
  final int? id;

  @JsonKey()
  final List<Emoji>? emojis;

  @JsonKey(defaultValue: '', name: 'text_detractors')
  final String? textDetractors;

  @JsonKey(defaultValue: '', name: 'text_passives')
  final String? textPassives;

  @JsonKey(defaultValue: '', name: 'text_promoters')
  final String? textPromoters;

  NpsEmojiList({
    this.id,
    this.textDetractors,
    this.textPassives,
    this.textPromoters,
    this.emojis,
  });

  factory NpsEmojiList.fromJson(Map<String, dynamic> json) => _$NpsEmojiListFromJson(json);
}
