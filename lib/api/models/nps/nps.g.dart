// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'nps.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Nps _$NpsFromJson(Map<String, dynamic> json) {
  return Nps(
    id: json['id'] as int?,
    text: json['text'] as String? ?? '',
    npsEmojis: json['nps_emojis'] == null
        ? null
        : NpsEmojiList.fromJson(json['nps_emojis'] as Map<String, dynamic>),
  );
}

NpsEmojiList _$NpsEmojiListFromJson(Map<String, dynamic> json) {
  return NpsEmojiList(
    id: json['id'] as int?,
    textDetractors: json['text_detractors'] as String? ?? '',
    textPassives: json['text_passives'] as String? ?? '',
    textPromoters: json['text_promoters'] as String? ?? '',
    emojis: (json['emojis'] as List<dynamic>?)
        ?.map((e) => Emoji.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}
