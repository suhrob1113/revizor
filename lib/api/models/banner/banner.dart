// Package imports:
import 'package:json_annotation/json_annotation.dart';

part 'banner.g.dart';

@JsonSerializable(createToJson: false)
class Banner {
  @JsonKey(defaultValue: '')
  final String? image;

  @JsonKey(name: 'banner_text', defaultValue: '')
  final String? bannerText;

  Banner({this.image, this.bannerText});

  factory Banner.fromJson(Map<String, dynamic> json) => _$BannerFromJson(json);
}
