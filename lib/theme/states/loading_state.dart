// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/conf/values/color_constants.dart';

class MyLoadingState extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Theme(
        data: ThemeData(accentColor: MyColors.accent),
        child: Center(child: CircularProgressIndicator()),
      ),
    );
  }
}
