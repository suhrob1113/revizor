// // Flutter imports:
// import 'package:flutter/material.dart';

// // Project imports:
// import 'package:osport_messages/theme/default/default_button.dart';
// import 'package:osport_messages/theme/theme.dart';

// class MyErrorState extends StatelessWidget {
//   final Function onTap;
//   final String message;
//   final String buttonText;

//   MyErrorState(this.onTap, this.message, {this.buttonText = 'ПОВТОРИТЬ'});

//   @override
//   Widget build(BuildContext context) {
//     return Expanded(
//       child: Padding(
//         padding: const EdgeInsets.symmetric(horizontal: 16),
//         child: Column(
//           mainAxisSize: MainAxisSize.min,
//           mainAxisAlignment: MainAxisAlignment.center,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [
//             Text(
//               '$message',
//               style: MyTheme.headline3,
//               textAlign: TextAlign.center,
//             ),
//             SizedBox(height: 16),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               crossAxisAlignment: CrossAxisAlignment.center,
//               children: [
//                 MyButton(
//                   text: buttonText,
//                   onTap: onTap,
//                   isLite: false,
//                 ),
//               ],
//             ),
//             SizedBox(height: 16),
//           ],
//         ),
//       ),
//     );
//   }
// }
