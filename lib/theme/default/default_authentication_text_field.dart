// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Project imports:
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class MyAuthenticationTextField extends StatelessWidget {
  final TextEditingController? controller;
  final String? labelText;
  final String? hintText;
  final TextInputType? keyboardType;
  final TextCapitalization? textCapitalization;
  final TextStyle? textStyle;

  final FocusNode? focus;

  final bool? enabled, autofocus, isLabel;
  final int? maxLength, maxLines;
  final double? letterSpacing;

  final Function(String)? onChanged;
  final Function()? onTap;
  final Function()? onEditingComplete;
  final Function(String)? onFieldSubmitted;

  final Widget? prefixIcon;
  final Widget? suffixIcon;

  final bool? acceptCommaAsDecimalPoint;
  final bool? obscureText;

  final TextAlign? textAlign;

  MyAuthenticationTextField({
    this.controller,
    this.labelText,
    this.hintText,
    this.textStyle,
    this.isLabel = false,
    this.enabled,
    this.autofocus = false,
    this.focus,
    this.keyboardType = TextInputType.text,
    this.textCapitalization = TextCapitalization.words,
    this.maxLength,
    this.maxLines = 1,
    this.onTap,
    this.onChanged,
    this.onEditingComplete,
    this.onFieldSubmitted,
    this.letterSpacing,
    this.acceptCommaAsDecimalPoint = false,
    this.obscureText = false,
    this.prefixIcon,
    this.suffixIcon,
    this.textAlign = TextAlign.start,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onTap: onTap,
      onEditingComplete: onEditingComplete,
      enabled: enabled,
      autofocus: autofocus!,
      focusNode: focus,
      controller: controller,
      maxLines: maxLines,
      obscureText: obscureText!,
      textAlign: textAlign!,
      decoration: InputDecoration(
        //    contentPadding: const EdgeInsets.only(left: 12),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: SPHelper.themeId == 1
                ? MyColors.grey30
                : SPHelper.themeId == 2
                    ? MyColors.primary35
                    : MyColors.grey30,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: SPHelper.themeId == 1
                ? MyColors.white
                : SPHelper.themeId == 2
                    ? MyColors.primary
                    : MyColors.white,
          ),
        ),
        prefixIcon: prefixIcon,

        hintText: hintText,
        hintStyle: TextStyle(
          fontSize: 22,
          color: SPHelper.themeId == 1
              ? MyColors.grey30
              : SPHelper.themeId == 2
                  ? MyColors.primary35
                  : MyColors.white30,
        ),
        suffixIcon: suffixIcon,
      ),
      style: textStyle ??
          TextStyle(
            fontSize: 22,
            color: SPHelper.themeId == 1
                ? MyColors.white
                : SPHelper.themeId == 2
                    ? MyColors.primary
                    : MyColors.white,
          ),
      cursorColor: MyColors.primary,
      keyboardType: keyboardType,
      textCapitalization: textCapitalization!,
      onChanged: onChanged,
      onFieldSubmitted: onFieldSubmitted,
    );
  }
}
