// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/theme/box_decorations.dart';

class MyContainer extends StatelessWidget {
  final double? width, height;
  final EdgeInsetsGeometry? margin, padding;

  final Color? color;
  final List<BoxShadow>? boxShadow;
  final BoxBorder? border;
  final BorderRadiusGeometry? borderRadius;

  final Function()? onTap;
  final Widget? child;

  final bool? isRounded;
  final bool? hasShadow;

  const MyContainer({
    Key? key,
    this.width,
    this.height,
    this.margin,
    this.padding = const EdgeInsets.all(0),
    this.color = MyColors.white,
    this.boxShadow,
    this.border,
    this.borderRadius,
    this.onTap,
    this.isRounded = false,
    this.hasShadow = false,
    this.child,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: width,
        height: height,
        margin: margin,
        padding: padding,
        decoration: BoxDecoration(
          color: color,
          boxShadow: (hasShadow ?? false) ? MyBoxShadow.shadow : boxShadow,
          border: border,
          borderRadius: (isRounded ?? false) ? MyBorderRadius.allRounded15 : borderRadius,
        ),
        child: child,
      ),
    );
  }
}
