// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/conf/assets/icon_constants.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/box_decorations.dart';
import 'package:revizor/utils/shared_preference_helper.dart';
import 'default_container.dart';
import 'default_icon_asset.dart';
import 'default_text.dart';

class MyDialog extends StatelessWidget {
  final String title;
  final Widget child;
  final bool hasTitleAndBack;

  const MyDialog({Key? key, this.title = '', required this.child, this.hasTitleAndBack = true}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: MyBorderRadius.allRounded15,
      ),
      child: MyContainer(
        padding: const EdgeInsets.all(32),
        isRounded: true,
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        color: SPHelper.themeId == 1
            ? MyColors.sailor
            : SPHelper.themeId == 2
                ? MyColors.white
                : MyColors.accent,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            if (hasTitleAndBack)
              Row(
                children: [
                  MyIconAsset(
                    MyIcons.back,
                    onTap: onBack,
                    size: 33,
                    color: SPHelper.themeId == 1
                        ? MyColors.white
                        : SPHelper.themeId == 2
                            ? MyColors.primary
                            : MyColors.white,
                  ),
                  Spacer(),
                  MyText(
                    title,
                    fontSize: 26,
                    fontWeight: FontWeight.w700,
                    textAlign: TextAlign.center,
                  ),
                  Spacer(),
                  SizedBox(
                    width: 33,
                  ),
                ],
              ),
            SizedBox(height: 10),
            child,
          ],
        ),
      ),
    );
  }

  void onBack() {
    RootService.appraisal.prolongAndSend();
    RootService.navigator.pop();
  }
}
