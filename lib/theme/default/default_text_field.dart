// Dart imports:
import 'dart:ui';

// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Package imports:
import 'package:outline_gradient_button/outline_gradient_button.dart';

// Project imports:
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/conf/values/gradient_constrants.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class MyTextField extends StatelessWidget {
  final TextEditingController? controller;
  final String? labelText;
  final String? hintText;
  final TextInputType? keyboardType;
  final TextCapitalization? textCapitalization;
  final TextStyle? textStyle;

  final FocusNode? focus;

  final bool? enabled, autofocus, isLabel;
  final int? maxLength, maxLines, minLines;
  final double? letterSpacing;

  final Function(String)? onChanged;
  final Function()? onTap;
  final Function()? onEditingComplete;
  final Function()? onSubmitted;

  final Widget? prefixIcon;
  final Widget? suffixIcon;

  final bool? acceptCommaAsDecimalPoint;
  final bool? obscureText;

  final TextAlign? textAlign;

  final List<TextInputFormatter>? inputFormatters;

  final String? prefixText;
  final TextStyle? prefixStyle;

  MyTextField({
    this.controller,
    this.labelText,
    this.hintText,
    this.textStyle,
    this.isLabel = false,
    this.enabled,
    this.autofocus = false,
    this.focus,
    this.keyboardType = TextInputType.text,
    this.textCapitalization = TextCapitalization.words,
    this.maxLength,
    this.maxLines,
    this.minLines,
    this.onTap,
    this.onChanged,
    this.onEditingComplete,
    this.onSubmitted,
    this.letterSpacing,
    this.acceptCommaAsDecimalPoint = false,
    this.obscureText = false,
    this.prefixIcon,
    this.suffixIcon,
    this.textAlign = TextAlign.start,
    this.inputFormatters,
    this.prefixText,
    this.prefixStyle,
  });

  @override
  Widget build(BuildContext context) {
    return OutlineGradientButton(
      gradient: SPHelper.themeId == 3
          ? LinearGradient(colors: [MyColors.transparent, MyColors.transparent])
          : MyGradients.primaryGradient,
      strokeWidth: 1,
      radius: Radius.circular(15),
      child: TextFormField(
        onTap: onTap,
        onEditingComplete: onEditingComplete,
        enabled: enabled,
        autofocus: autofocus!,
        focusNode: focus,
        controller: controller,
        maxLines: maxLines,
        minLines: minLines,
        obscureText: obscureText!,
        textAlign: textAlign!,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.all(12),
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          // border: SPHelper.themeId == 3
          //     ? InputBorder.none
          //     : GradientOutlineInputBorder(
          //         gapPadding: 1,
          //         focusedGradient: MyGradients.primaryGradient,
          //         unfocusedGradient: MyGradients.primaryGradient,
          //         borderRadius: MyBorderRadius.allRounded15,
          //         borderSide: BorderSide(width: 1),
          //       ),
          prefixIcon: prefixIcon,
          prefixText: prefixText,
          prefixStyle: prefixStyle ??
              TextStyle(
                fontSize: 22,
                color: SPHelper.themeId == 1
                    ? MyColors.white
                    : SPHelper.themeId == 2
                        ? MyColors.primary
                        : MyColors.white,
              ),
          hintText: hintText,
          hintStyle: TextStyle(fontSize: 22, color: MyColors.primary35),
          suffixIcon: suffixIcon,
        ),
        style: textStyle ??
            TextStyle(
              fontSize: 22,
              color: SPHelper.themeId == 1
                  ? MyColors.white
                  : SPHelper.themeId == 2
                      ? MyColors.primary
                      : MyColors.white,
            ),
        cursorColor: SPHelper.themeId == 1
            ? MyColors.white
            : SPHelper.themeId == 2
                ? MyColors.primary
                : MyColors.white,
        keyboardType: keyboardType,
        textCapitalization: textCapitalization!,
        onChanged: onChanged,
        inputFormatters: inputFormatters,
      ),
    );
  }
}
