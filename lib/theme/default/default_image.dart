// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shimmer/shimmer.dart';

// Project imports:
import 'package:revizor/conf/values/color_constants.dart';

// Project imports:

class MyImage extends StatelessWidget {
  final String imgUrl;
  final double? width, height;
  final BoxFit? fit;
  final Alignment? alignment;

  bool get isSVG => (imgUrl).endsWith('.svg');

  MyImage(
    this.imgUrl, {
    this.width,
    this.height,
    this.fit,
    this.alignment = Alignment.center,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: isSVG
          ? SvgPicture.network(
              imgUrl,
              placeholderBuilder: (_) => getPlaceholder(),
              width: width,
              height: height,
              fit: fit ?? BoxFit.contain,
              alignment: alignment!,
            )
          : CachedNetworkImage(
              imageUrl: imgUrl,
              width: width,
              height: height,
              fit: fit ?? BoxFit.contain,
              alignment: alignment!,
              placeholder: (_, url) => getPlaceholder(),
              errorWidget: (_, url, d) => getPlaceholder(),
            ),
    );
  }

  Widget getPlaceholder() {
    return Shimmer.fromColors(
      baseColor: Colors.grey[300]!,
      highlightColor: Colors.grey[100]!,
      direction: ShimmerDirection.ltr,
      child: Container(
        width: width,
        height: height,
        color: MyColors.grey,
      ),
    );
  }
}
