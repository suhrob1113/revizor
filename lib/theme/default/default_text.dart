// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class MyText extends StatelessWidget {
  final data;

  final double? fontSize, height, letterSpacing;
  final FontWeight? fontWeight;
  final Color? color;

  final bool? isOverflow;
  final int? maxLines;
  final TextAlign? textAlign;

  final Function()? onTap;

  MyText(
    this.data, {
    this.fontSize,
    this.height,
    this.letterSpacing,
    this.fontWeight = FontWeight.w400,
    this.color,
    this.maxLines,
    this.textAlign,
    this.isOverflow = false,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Text(
        data,
        style: TextStyle(
          color: color ??
              (SPHelper.themeId == 1
                  ? MyColors.white
                  : SPHelper.themeId == 2
                      ? MyColors.primary
                      : MyColors.white),
          height: height,
          fontSize: fontSize,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing,
        ),
        overflow: isOverflow! ? TextOverflow.ellipsis : null,
        maxLines: maxLines,
        textAlign: textAlign,
      ),
    );
  }
}
