// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_svg/flutter_svg.dart';

// Project imports:
import 'package:revizor/conf/values/color_constants.dart';
import 'default_container.dart';

class MyIconNetwork extends StatelessWidget {
  final String icon;

  /// Size будет игнорироваться если задать стороны
  final double? size;
  final double? width;
  final double? height;

  final Function()? onTap;
  final EdgeInsetsGeometry? padding;
  final Color? color, backgroundColor;
  final bool? isRounded, isCentered;
  final BoxBorder? border;
  final List<BoxShadow>? boxShadow;

  MyIconNetwork(
    this.icon, {
    this.size,
    this.width,
    this.height,
    this.onTap,
    this.padding = const EdgeInsets.all(0),
    this.color,
    this.backgroundColor = MyColors.transparent,
    this.isRounded = false,
    this.isCentered = true,
    this.border,
    this.boxShadow = const [],
  });

  @override
  Widget build(BuildContext context) {
    return MyContainer(
      onTap: onTap,
      padding: padding,
      border: border,
      isRounded: isRounded,
      boxShadow: boxShadow,
      color: backgroundColor,
      width: width ?? size,
      height: height ?? size,
      child: center(
        child: SvgPicture.network(
          '$icon',
          width: width ?? size,
          height: height ?? size,
          color: color,
        ),
      ),
    );
  }

  Widget center({child}) {
    if (isCentered ?? false)
      return Center(child: child);
    else
      return child;
  }
}
