// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/conf/assets/icon_constants.dart';
import 'package:revizor/conf/assets/image_constants.dart';
import 'package:revizor/conf/routes/main_routes_constants.dart';
import 'package:revizor/conf/sp_keys/shared_preference_constants.dart';
import 'package:revizor/conf/values/color_constants.dart';
import 'package:revizor/services/root_service.dart';
import 'package:revizor/theme/default/default_container.dart';
import 'package:revizor/theme/default/default_icon_asset.dart';
import 'package:revizor/theme/default/default_image.dart';
import 'package:revizor/theme/default/default_text.dart';
import 'package:revizor/utils/shared_preference_helper.dart';

class MyAppbar extends PreferredSize {
  final String title;
  final String? imgUrl;
  final bool hasShadow;

  MyAppbar(
    this.title, {
    this.hasShadow = false,
    this.imgUrl,
  }) : super(preferredSize: Size.fromHeight(76), child: Container());

  @override
  Widget build(BuildContext context) {
    return MyContainer(
      color: SPHelper.themeId == 1
          ? MyColors.sailor
          : SPHelper.themeId == 2
              ? MyColors.white
              : MyColors.accent,
      hasShadow: hasShadow,
      padding: const EdgeInsets.symmetric(vertical: 22, horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          if (imgUrl != null)
            MyImage(
              imgUrl!,
              height: 32,
            ),
          MyText(
            title,
            fontSize: 26,
            height: 32 / 26,
            fontWeight: FontWeight.w700,
          ),
          MyIconAsset(
            MyIcons.logo,
            width: 103,
            height: 32,
            color: SPHelper.themeId == 1
                ? MyColors.white
                : SPHelper.themeId == 2
                    ? MyColors.accent
                    : MyColors.white,
          )
        ],
      ),
    );
  }
}
