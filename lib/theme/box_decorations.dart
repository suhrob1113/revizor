// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/conf/values/color_constants.dart';

// Project imports:

class MyBoxShadow {
  //
  //
  static const List<BoxShadow> shadow = [
    BoxShadow(
      color: MyColors.shadow,
      offset: Offset(5, 20),
      blurRadius: 20,
      spreadRadius: 0,
    )
  ];
}

// class MyGradients {
//   //
//   //
//   static const LinearGradient portfolioCard = LinearGradient(
//     begin: Alignment.topCenter,
//     end: Alignment.bottomCenter,
//     colors: [
//       MyColors.lightBueGradientColor1,
//       MyColors.lightBueGradientColor2,
//       MyColors.lightBueGradientColor3,
//     ],
//     stops: [
//       0,
//       0.4688,
//       1,
//     ],
//   );

//   static const LinearGradient inputUnfocusedGradientLight = LinearGradient(
//     begin: Alignment.topCenter,
//     end: Alignment.bottomCenter,
//     colors: [
//       MyColors.grey4,
//       MyColors.grey4,
//     ],
//   );

//   static const LinearGradient selectedBottomNavIcon = LinearGradient(
//     begin: Alignment.topCenter,
//     end: Alignment.bottomCenter,
//     colors: [
//       MyColors.selectedBottomNavIconGradientColor1,
//       MyColors.selectedBottomNavIconGradientColor2,
//     ],
//     stops: [
//       0,
//       1,
//     ],
//   );
//   static const LinearGradient unselectedBottomNavIcon = LinearGradient(
//     begin: Alignment.topCenter,
//     end: Alignment.bottomCenter,
//     colors: [
//       MyColors.unselectedBottomNavIconGradientColor1,
//       MyColors.unselectedBottomNavIconGradientColor2,
//     ],
//     stops: [
//       0,
//       1,
//     ],
//   );

// static const LinearGradient portfolioCard = LinearGradient(
//   begin: Alignment.bottomLeft,
//   end: Alignment.topRight,
//   colors: <Color>[
//     MyColors.gradientBlue,
//     MyColors.gradientViolet,
//   ],
// );
//}

class MyBorderRadius {
  //
  //
  static const allRounded15 = const BorderRadius.all(Radius.circular(15));
  static const allRounded50 = const BorderRadius.all(Radius.circular(50));
  static const allRounded150 = const BorderRadius.all(Radius.circular(150));

  static const topRounded = BorderRadius.only(
    topLeft: const Radius.circular(4),
    topRight: const Radius.circular(4),
  );

  static const bottomRounded = BorderRadius.only(
    bottomLeft: const Radius.circular(10),
    bottomRight: const Radius.circular(10),
  );

  static const popUpRounded = RoundedRectangleBorder(
    borderRadius: MyBorderRadius.topRounded,
  );
}
