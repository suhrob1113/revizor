// Dart imports:
import 'dart:math';

// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:revizor/conf/values/gradient_constrants.dart';

class RipplesAnimation extends CustomPainter {
  final double progress;
  final int count;
  final Color color;

  Gradient myGradient = MyGradients.primaryGradient;

  Paint _paint = Paint();

  RipplesAnimation(this.progress,
      {this.count = 3, this.color = const Color(0xFFFFFFFF)});

  @override
  void paint(Canvas canvas, Size size) {
    double radius = min(size.width / 2, size.height / 2);
    var rect = Offset.zero & size;

    for (int i = count; i >= 0; i--) {
      final double opacity = (1.0 - ((i + progress) / (count + 1)));

      final Color _color = color.withOpacity(opacity);
      _paint..color = _color;

      _paint..shader = MyGradients.primaryGradient.createShader(rect);

      double _radius = radius * ((i + progress) / (count + 1));

      canvas.drawCircle(
          Offset(size.width / 2, size.height / 2), _radius, _paint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
