// Dart imports:
import 'dart:ui';

// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Project imports:

abstract class MyTheme {
  // static final datePickerTheme = DatePickerTheme(
  //   cancelStyle: TextStyle(
  //     fontSize: 16,
  //     height: 19 / 16,
  //     fontFamily: 'Mulish',
  //     fontWeight: FontWeight.w700,
  //     color: MyColors.ligthBlue,
  //   ),
  //   doneStyle: TextStyle(
  //     fontSize: 16,
  //     height: 19 / 16,
  //     fontFamily: 'Mulish',
  //     fontWeight: FontWeight.w700,
  //     color: MyColors.accent,
  //   ),
  //   itemStyle: TextStyle(
  //     fontSize: 16,
  //     height: 19 / 16,
  //     fontFamily: 'Mulish',
  //     fontWeight: FontWeight.w400,
  //     color: SPHelper.isDark ? MyColors.white : MyColors.greyDark,
  //   ),
  //   backgroundColor: SPHelper.isDark ? MyColors.black : MyColors.white,
  // );
}

setSystemUiOverlays() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
    statusBarBrightness: Brightness.light,
    statusBarIconBrightness: Brightness.light,
    systemNavigationBarColor: Colors.deepPurple,
    systemNavigationBarIconBrightness: Brightness.light,
    systemNavigationBarDividerColor: Colors.deepPurple,
  ));
}

// void setTheme({Color bottom}) {
//   if (SPHelper.isDark)
//     setDarkSystemUiOverlays(bottom ?? MyColors.dark);
//   else
//     setLiteSystemUiOverlays(bottom ?? MyColors.white);
// }

// setLiteSystemUiOverlays(Color bottom) async {
//   SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//     statusBarColor: MyColors.transparent,
//     statusBarBrightness: Brightness.light,
//     statusBarIconBrightness: Brightness.dark,
//     systemNavigationBarColor: bottom,
//     systemNavigationBarIconBrightness: Brightness.dark,
//     systemNavigationBarDividerColor: bottom,
//   ));
// }

// setDarkSystemUiOverlays(Color bottom) async {
//   SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//     statusBarColor: MyColors.transparent,
//     statusBarBrightness: Brightness.dark,
//     statusBarIconBrightness: Brightness.light,
//     systemNavigationBarColor: bottom,
//     systemNavigationBarIconBrightness: Brightness.light,
//     systemNavigationBarDividerColor: bottom,
//   ));
//}
