abstract class MyRoutes {
  static const authenticationPage = 'authenticationPage';
  static const departmentsPage = 'departmentsPage';
  static const employeesPage = 'employeesPage';
  static const rateServicePage = 'rateServicePage';
  static const rateWithBarner = 'rateWithBarner';
  static const rateWithChoice = 'rateWithChoice';
  static const rateWithNps = 'rateWithNps';
  static const cameraPage = 'cameraPage';
  static const webCameraPage = 'webCameraPage';
}
