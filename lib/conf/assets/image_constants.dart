// Project imports:

// Project imports:
import 'assets_contstants.dart';

abstract class MyImages {
  static const banner = '${MyAssets.images}/banner.png';
  static const departments = '${MyAssets.images}/departments.png';

  static const salute = '${MyAssets.images}/salute.png';

  static const doc1 = '${MyAssets.images}/doc1.png';
  static const doc2 = '${MyAssets.images}/doc2.png';
  static const doc3 = '${MyAssets.images}/doc3.png';
  static const doc4 = '${MyAssets.images}/doc4.png';
  static const doc5 = '${MyAssets.images}/doc5.png';
  static const doc6 = '${MyAssets.images}/doc6.png';
  static const doc7 = '${MyAssets.images}/doc7.png';
  static const doc8 = '${MyAssets.images}/doc8.png';
  static const doc9 = '${MyAssets.images}/doc9.png';
  static const doc10 = '${MyAssets.images}/doc10.png';

  static String doc(int n) => '${MyAssets.images}/doc$n.png';

  static const logo2 = '${MyAssets.images}/logo2.png';

  static const pause = '${MyAssets.images}/pause.png';
  static const pauseWhite = '${MyAssets.images}/pause-white.png';
}
