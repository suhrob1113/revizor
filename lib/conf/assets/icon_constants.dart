// Project imports:

// Project imports:
import 'assets_contstants.dart';

abstract class MyIcons {
  // Common
  static const back = '${MyAssets.icons}/back.svg';
  static const down = '${MyAssets.icons}/down.svg';
  static const up = '${MyAssets.icons}/up.svg';

  static const eng = '${MyAssets.icons}/eng.svg';
  static const ru = '${MyAssets.icons}/ru.svg';
  static const uzb = '${MyAssets.icons}/uzb.svg';

  static const logo = '${MyAssets.icons}/logo.svg';
  static const logo2 = '${MyAssets.icons}/logo2.svg';

  static const smile1 = '${MyAssets.icons}/smiley1.svg';
  static const smile2 = '${MyAssets.icons}/smiley2.svg';
  static const smile3 = '${MyAssets.icons}/smiley3.svg';
  static const smile4 = '${MyAssets.icons}/smiley4.svg';
  static const smile5 = '${MyAssets.icons}/smiley5.svg';

  static const smileSad = '${MyAssets.icons}/smiley_sad.svg';

  static const starOn = '${MyAssets.icons}/star-on.svg';
  static const starOff = '${MyAssets.icons}/star-off.svg';

  static const denied = '${MyAssets.icons}/denied.svg';
  static const takeVideo = '${MyAssets.icons}/take-video.svg';
  static const record = '${MyAssets.icons}/record.svg';
  static const pause = '${MyAssets.icons}/pause.svg';
  static const pauseWhite = '${MyAssets.icons}/pause-white.svg';
  static const write = '${MyAssets.icons}/write.svg';

  static const email = '${MyAssets.icons}/email.svg';
  static const password = '${MyAssets.icons}/password.svg';

  static String flag(String name) => '${MyAssets.icons}/' + name.toLowerCase() + '.svg';

  static String smiley(int id) => '${MyAssets.icons}/smiley$id.svg';
}
