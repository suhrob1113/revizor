abstract class MyDateFormats {
  /// dd.MM.yyyy
  static const tDateFormat1 = 'dd.MM.yyyy';

  /// d MMM yyyy
  static const tDateFormat2 = 'd MMM yyyy';

  /// d MMMM, yyyy
  static const tDateFormat3 = 'd MMM \n yyyy';

  /// dd.MM.yyyy
  static const tDateFormat4 = 'dd.MM.yyyy';

  /// HH:mm
  static const tDateFormat5 = 'HH:mm';

  /// yyyy-MM-dd
  static const tDateFormat6 = 'yyyy-MM-dd';

  /// dd MMM yyyy в HH:mm
  static const tDateFormat7 = 'MMM dd, yyyy';

  /// yyyy-dd-MM
  static const tDateFormat8 = 'yyyy-dd-MM';

  //MMM dd, yyyy
  static const tDateFormat9 = 'MMM dd, yyyy';

  //dd/MM/yyyy HH:mm
  static const tDateFormat10 = 'dd/MM/yyyy HH:mm';
}
