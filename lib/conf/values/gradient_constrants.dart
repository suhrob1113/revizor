// Flutter imports:
import 'package:flutter/cupertino.dart';

// Project imports:
import 'package:revizor/conf/values/color_constants.dart';

class MyGradients {
  static const primaryGradient = LinearGradient(
    colors: [
      MyColors.primaryGradient1,
      MyColors.primaryGradient2,
    ],
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
  );
}
