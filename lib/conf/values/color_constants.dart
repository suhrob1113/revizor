// Dart imports:
import 'dart:ui';

abstract class MyColors {
  //Theme 1
  static const dark = Color(0xFF192C54);
  static const sailor = Color(0xFF1C3B6A);
  static const sailorLight = Color(0xFF2F5081);
  static const grey30 = Color(0x30E6D9F6);
  static const white30 = Color(0x30FFFFFF);
  static const white40 = Color(0x40FFFFFF);
  static const accentDark = Color(0xFF6B12DD);

  //Theme 3

  //Theme 2
  static const transparent = Color(0x00000000);

  static const primary = Color(0xFF322F56);

  static const primary35 = Color(0x35322F56);

  static const accent = Color(0xFF7800FF);

  static const white = Color(0xFFFFFFFF);

  static const grey = Color(0xFFF8F8FA);

  static const black = Color(0xFF000000);

  static const shadow = Color(0x157090B0);

  static const red = Color(0xFFDC2863);

  static const yellow = Color(0xFFF8DC25);
  static const yellowDarker = Color(0xFFFFBF0B);

  static const green = Color(0xFF79C045);

  static const greyTransparent = Color(0x38B6C4CF);

  static const primaryGradient1 = Color(0xFFB851FF);
  static const primaryGradient2 = Color(0xFF19B4F8);
}
