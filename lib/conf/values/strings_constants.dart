abstract class MyStrings {
  static const appName = 'Lagom';
  static const welcome = 'Добро пожаловать!';
  static const enter = 'Войти';

  static const rateOurService = 'Оцените качество нашего сервиса';
  static const selectDepartMent = 'Выберите направление';
  static const chooseDoc = 'Выберите врача, у которого Вы были на приеме';
  static const surgery = 'Хирургия';
  static const rateEmployee = 'Пожалуйста, оцените качество приема у врача';
  static const rateService = 'Пожалуйста, оцените\nкачество предоставляемой услуги';
  static const didntLikedToday = 'Что вам сегодня не понравилось?';
  static const didntLikedTodayLeaveRecord = 'Что вам сегодня не понравилось? Оставьте аудиозапись';
  static const didntLikedTodayLeaveVideo = 'Что вам сегодня не понравилось? Оставьте видеозапись';
  static const didntLikedTodayLeaveComment = 'Что вам сегодня не понравилось? Оставьте комментарий';

  static const wantToRecord = 'Хочу сказать';
  static const wantToTakeVideo = 'Хочу записать видео';
  static const wantToWrite = 'Хочу написать';
  static const noComments = 'Без комментариев';

  static const leavePhoneNumber = '*Оставить номер телефона';
  static const comment = '*Комментарий';
  static const send = 'Отправить';
  static const speak = 'Говорите';
  static const ok = 'ОК';

  static const thankYou = 'Спасибо!';
  static const only4people =
      '\"Только один из 4 человек решается\nоставить отзыв, спасибо что делаете сервис\nв Узбекистане лучше!\"';

  static const helpUsBeBetter = 'Помогите Нам Стать Лучше!';

  static const totalDocRating = 'Общий рейтинг';
  static const ratingsCount = 'Число проголосовавших';

  static const howWouldYouRateOurOrganization = 'Как бы вы оценили нашу организацию? ';

  static const Map<int, String> smileys = {1: 'Ужасно', 2: 'Плохо', 3: 'Нормально', 4: 'Хорошо', 5: 'Отлично'};
}
