abstract class MyUrl {
  // Base
  static const baseUrl = 'https://lagoms.uz/api/thetablet';
  static const account = '/account/';
  static const systextList = '/systext/list/';
  static const appraisal = '/appraisal/';
  static const data = '/data/';
}
