// Shared Preference Keys

abstract class SPKeys {
  static const String themeId = 'themeId';
  static const String accessToken = 'accessToken';
  static const String accessType = 'accessType';
  static const String language = 'language';
  static const String unsuccessfulAppraisalList = 'unsuccessfulAppraisalList';
}
