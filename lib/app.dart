// Flutter imports:
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';

// Package imports:
import 'package:wakelock/wakelock.dart';

// Project imports:
import 'package:revizor/conf/routes/main_routes_constants.dart';
import 'package:revizor/utils/shared_preference_helper.dart';
import 'conf/values/color_constants.dart';
import 'conf/values/font_constants.dart';
import 'conf/values/strings_constants.dart';
import 'services/root_service.dart';
import 'utils/media_helper.dart';
import 'utils/my_scroll_behavior.dart';
import 'utils/navigator/main_route_generator.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  @override
  void initState() {
    WidgetsBinding.instance!.addObserver(this);
    if (!kIsWeb) Wakelock.enable();
    super.initState();
  }

  @override
  void dispose() {
    if (!kIsWeb) Wakelock.disable();
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: MyStrings.appName,
      theme: ThemeData(
        fontFamily: MyFonts.mainFont,
        accentColor: MyColors.primary,
        primaryColor: MyColors.primary,
        backgroundColor: SPHelper.themeId == 1
            ? MyColors.dark
            : SPHelper.themeId == 2
                ? MyColors.grey
                : MyColors.white,
      ),
      builder: builder,
      initialRoute: intitialRoute(),
      navigatorKey: RootService.navigator.key,
      onGenerateRoute: MainRouteGenerator.generateRoute,
      debugShowCheckedModeBanner: false,
    );
  }

  String intitialRoute() {
    if (SPHelper.accessToken != null) {
      switch (SPHelper.accessType) {
        case 1:
          return MyRoutes.rateServicePage;
        case 2:
          return MyRoutes.employeesPage;
        case 3:
          return MyRoutes.departmentsPage;
        case 4:
          return MyRoutes.rateWithBarner;
        case 5:
          return MyRoutes.rateWithBarner;
        case 6:
          return MyRoutes.rateWithChoice;
        case 7:
          return MyRoutes.rateWithNps;
        default:
          return MyRoutes.authenticationPage;
      }
    } else {
      return MyRoutes.authenticationPage;
    }
  }

  Widget builder(context, child) {
    MediaHelper.init(context);

    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: ScrollConfiguration(
        behavior: MyScrollBehavior(),
        child: SafeArea(
          child: child,
        ),
      ),
    );
  }
}
